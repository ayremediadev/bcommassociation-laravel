<?php 

echo 'This is the subscribe process script - if you see this, the form is directing correctly
        <br/> This will not be seen in production.
        <br/><a href="http://bcommalaratest.ga/laravel/public/">Go back to homepage</a>
        <br/<br/><br/>';

echo URL::to('/') . '<br/>'; 

echo 'Email: ' . $_POST['email'] . '<br/>';
echo 'First name: ' . $_POST['firstname'] . '<br/>';
echo 'Last name: ' . $_POST['lastname'] . '<br/>'; 
echo 'Date: ' . date('Y-m-d'); 

DB::table('subscribers')->insert(
    ['email' => $_POST['email'],  
    'fullname' => $_POST['firstname'] . ' ' . $_POST['lastname'], 
    'phone' => NULL, 
    'message' => NULL, 
    'join_assoc' => NULL, 
    'subscribe' => 'Yes', 
    'categories' => NULL, 
    'company' => NULL, 
    'date' => date('Y-m-d')]
);