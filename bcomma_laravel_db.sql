-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 18, 2018 at 11:28 PM
-- Server version: 5.6.41
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bcommala_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `bcomm_connections`
--

CREATE TABLE `bcomm_connections` (
  `user_id_1` mediumint(9) NOT NULL,
  `user_id_2` mediumint(9) NOT NULL,
  `connection` tinyint(4) NOT NULL,
  `approver_id` mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bcomm_events`
--

CREATE TABLE `bcomm_events` (
  `event_id` mediumint(9) NOT NULL,
  `event_title` mediumtext NOT NULL,
  `event_content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bcomm_jobs`
--

CREATE TABLE `bcomm_jobs` (
  `job_id` mediumint(9) NOT NULL,
  `job_title` mediumtext NOT NULL,
  `job_content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bcomm_messages`
--

CREATE TABLE `bcomm_messages` (
  `user_id_1` int(11) NOT NULL,
  `user_id_2` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `sent` datetime NOT NULL,
  `seen` datetime NOT NULL,
  `subject` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bcomm_pages`
--

CREATE TABLE `bcomm_pages` (
  `page_id` mediumint(9) NOT NULL,
  `page_title` mediumtext NOT NULL,
  `page_content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bcomm_pages`
--

INSERT INTO `bcomm_pages` (`page_id`, `page_title`, `page_content`) VALUES
(1, 'Community', ''),
(2, 'Membership', ''),
(3, 'About', '');

-- --------------------------------------------------------

--
-- Table structure for table `bcomm_settings`
--

CREATE TABLE `bcomm_settings` (
  `seo_events` tinyint(4) NOT NULL,
  `seo_jobs` tinyint(4) NOT NULL,
  `custom_styles` longtext NOT NULL,
  `profile_urls` tinyint(4) NOT NULL,
  `public_jobs` tinyint(4) NOT NULL,
  `public_events` tinyint(4) NOT NULL,
  `custom_tags` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bcomm_support`
--

CREATE TABLE `bcomm_support` (
  `ticket_id` mediumint(9) NOT NULL,
  `ticket_status` int(11) NOT NULL,
  `ticket_summary` text NOT NULL,
  `ticket_tag` smallint(6) NOT NULL,
  `ticket_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bcomm_tags`
--

CREATE TABLE `bcomm_tags` (
  `tag_id` int(11) NOT NULL,
  `tag_name` text NOT NULL,
  `tag_users` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(8) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_status` varchar(12) NOT NULL DEFAULT 'active',
  `user_role` varchar(15) NOT NULL DEFAULT 'member',
  `updates` tinyint(1) NOT NULL DEFAULT '1',
  `position` tinytext,
  `company` text,
  `location` mediumtext,
  `presentation` longtext,
  `bch_profile` text,
  `interests` mediumtext,
  `opportunities` longtext,
  `experiences` longtext,
  `connections` int(11) DEFAULT '0',
  `messages` mediumint(9) DEFAULT '0',
  `remember_token` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `password`, `created_at`, `updated_at`, `user_status`, `user_role`, `updates`, `position`, `company`, `location`, `presentation`, `bch_profile`, `interests`, `opportunities`, `experiences`, `connections`, `messages`, `remember_token`) VALUES
(40, 'emmanuel@ayremedia.com', 'Emmanuel DURAN CAMPANA', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(37, 'hilly.ehrlich@gmail.com', 'HILLIARD EHRLICH', '447411409083', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(38, 'hilly.ehrlich@gmail.com', 'HILLIARD EHRLICH', '0511409083', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(31, 'Kenneth@ayremedia.com', 'Kenneth Haugaard', '07741725151', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(72, 'cashconsortium@gmail.com', 'Gabriel Cardona', '5109991414', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(34, 'bill@ayremedia.com', 'Bill Beatty', '+639189200964', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(35, 'bill@ayremedia.com', 'Bill Beatty', '+639189200964', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(36, 'bruce@bdgamble.com', 'Bruce Gamble', '7909960077', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(71, 'daniel@dynamo-led-displays.co.uk', 'Daniel Reynolds', '07786680351', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(47, 'douglaszjiang@icloud.com', 'Douglas Jiang', '+8618500296315', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(77, 'mark@ayremedia.com', 'tyjkbv fgbvbk', '08754', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(78, 'kahn.hood@bitcoinrewards.com.au', 'Kahn Hood', '+61476664026', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(75, 'Mark@ayremedia.com', 'Test Tester', '6789', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(79, 'frizzers@gmail.com', 'Dominic Frisby', '7717131931', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(80, 'ros@agbrief.com', 'Rosalind Wade', '09171670636', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(81, 'widya@cryptartica.com', 'Widya Salim', '+447871626947', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(82, 'roman.puhek@gmail.com', 'Roman Puhek', '+38631665373', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(83, 'rob@crypto-studio.io', 'Rob Harris', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(84, 'george@thebitcoincash.fund', 'George  Samuels ', '+61439339696', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(85, 'thomascnolte.esq@gmail.com', 'Thomas Nolte', '5188791787', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(86, 'dale@thebitcoindoco.com', 'Dale Dickins', '+61405776169', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(87, 'esthonm@gmail.com', 'Esthon Medeiros', '+5521999458555', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(88, 'boris.chan@ttg.cn', 'Boris Chan', '+85296525988', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(89, 'e.climent@optusnet.com.au', 'Ethel Climent', '0401357210', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(90, 'Kurt@cryptotraderspro.com', 'Kurt Wuckert Jr', '16303731469', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(91, 'eilon@coinpoint.net', 'Eilon Arad', '639178544403', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(92, 'stephan@unisot.io', 'Stephan Nilsson', '+4790262465', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(93, 'Rohan@augx.com', 'Rohan  Thompson ', '0403996969', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(94, 'alexander@quickbit.eu', 'Alexander Hansson', '+34674723132', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(95, 'steffen.solling@gmail.com', 'Steffen SÃ¸lling', '60896670', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(96, 'dymurray@redhat.com', 'Dylan Murray', '8033727556', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(97, 'edward.laoliu@qq.com', 'Edward Liu', '+8613662275645', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(98, 'edward@bitcoin.com', 'C Edward Kelso', '6194961543', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(99, 'Lorien@Centbee.com', 'Lorien Gamaroff', '+27834451897', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(100, 'poetry4bitcoin@gmail.com', 'Andreas Olivas', '3106919024', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(101, '057.alex@gmail.com', 'Alex Ne', '+972537318418', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(102, 'angus@centbee.com', 'Angus Brown', '+27798722922', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(103, 'esthonm@gmail.com', 'Esthon Medeirios', '+5521999458555', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(104, 'mark@ayremedia.com', 'Mark Hebblewhite', '07771665128', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(105, 'mark@ayremedia.com', 'mark hebblewhite', '07771665128', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(160, 'steve@zawadi.co', 'Steve Gachau', '+254727959255', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(159, 'mark.winder@jetmark.co.uk', 'Mark Winder', '07966016898', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(109, 'esthon@rioblockchain.com.br', 'Esthon Medeiros', '+5521999458555', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(110, 'hayden@cryptostrategies.io', 'Hayden Otto', '+61490021800', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(111, 'gonzo@virtualpol.com', 'Javier GonzÃ¡lez GonzÃ¡lez', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(112, 'Brad1121@gmail.com ', 'Brad  Kristensen ', '+61412366899', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(113, 'info@tiptopwebdesign.com', 'Sandon Treweek', '+447876457319', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(114, 'lx@kralux.com', 'Alexandre Kral', '7148786648', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(115, 'duncanashworth@me.com', 'Duncan Ashworth', '+33621453261', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(116, 'alexgarnock@gmail.com', 'Alex Garnock', '0416469080', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(117, 'Darrellrideaux@gauntletholdings.com', 'Darrell Rideaux ', '013105976458', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(118, 'semyon@voltaire.cash', 'Semyon Germanovich', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(119, 'amadeo@cyber.capital', 'Amadeo Brands', '620741381', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(120, 'corpdev734@gmail.com', 'Phil Lawlor', '7342184055', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(121, 'james@nuentertainment.co', 'JAMES FINDLAY', '+447459204717', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(122, 'tester@marko.com', 'Tester Marko', '987665544', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(123, 'Andy@madabout.media', 'Andrew Edwards', '01514243619', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(124, 'BitcoinJerry@protonmail.com', 'Jerry French', '18179087026', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(125, 'seantr@hotmail.com', 'Sean Russell', '07770937220', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(126, 'nawaters@mac.com', 'Nick Waters', '07590193747', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(127, 'stevie@madabout.media', 'Stevie Woodrow', '07455140648', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(128, 'reina.magica@mailbox.org', 'Reina Magica', '+358442923138', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(129, 'info@designbuildengineeringpllc.com', 'Feliks Leybovich', '9172028368', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(130, 'oliver@chainvine.com', 'Oliver Oram', '0046734222313', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(131, 'neoncracklin@gmail.com', 'Scott Veronie', '9856473573', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(132, 'johnevancoe@gmail.com', 'John Evancoe', '9492334819', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(133, 'ingridshentptw@gmail.com', 'Ingrid Shen', '+886911395496', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(134, 'Shenol@shenol.com', 'Shenol Mustafov', '+359884619207', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(135, 'e.climent@optusnet.com.au', 'Ethel Climent', '0401357210', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(136, 'usatie@yenom.tech', 'Shun Usami', '+819053074836', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(137, 'Daniel.zammit@drzgroup.eu', 'Daniel Zammit ', '+35699443609', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(138, 'michael@apollocap.io', 'Michael Parnell', '+61418255543', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(139, 'mrclangtastic@hotmail.com', 'Jeff Guy', '+468783423', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(140, 'sheryl@catchinglight.tech', 'Sheryl Greentree', '01785286629', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(141, 'tbonillas@gmail.com', 'Gregory Bonillas', '9033904764', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(142, 'vee.tardrew@bitstocks.com', 'Vee Tardrew', '07549286201', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(161, 'management@tideafrica.com', 'Roger Munyampenda', '+250730198900', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(298, 'michellemdaigle@zoho.com', 'Michelle Daigle', '5145619295', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(163, 'carolyn@nuentertainment.co', 'CAROLYN HAMMOND', '+447459204717', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(162, 'james@nuentertainment.co', 'JAMES FINDLAY', '+447459204717', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(296, 'Joseph@vaughn.perling.com', 'Joseph VaughnPerling', '8188658243', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(297, 'Joseph@vaughn.perling.com', 'Joseph VaughnPerling', '8188658243', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(170, 'd886@protonmail.com', 'David Watson', '07788633801', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(171, 'kodaxx@gmail.com', 'Spencer Kuzara', '3027512344', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(255, 'pol@myeste.com', 'Pol Moreno Yeste', '00447596411220', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(254, 'Admin@d2dsales.co.uk', 'Paul Thompson', '07769549026', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(179, 'pablomartinez@aursis.es', 'Pablo MartÃ­nez', '34629555644', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(258, 'andygolay@gmail.com', 'Andy Golay', '7817750726', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(306, 'ash@fridaynightlabs.com', 'Ash Sharma', '5102904043', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(256, 'pol@myeste.com', 'Pol Moreno Yeste', '00447596411220', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(187, 'holger.vogel@bkb.ch', 'Holger Vogel', '+4915231740591', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(188, 'whulxh@gmail.com', 'Xiaohui Liu', '3133188421', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(259, 'Chris@bitcoin.com', 'Chris Ryan', '9053206744', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(309, 'mymaincontactis@gmail.com', 'Glen  Rogers', '0888136356', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(308, 'tshearer@cittris.my', 'Thomas Alan Shearer', '0060132840926', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(263, 'admin@cryptoindustries.ltd', 'shawn brewer', '610400029971', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(295, 'andrewmathes94@gmail.com', 'Andrew Mathes', '7706338697', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(305, 'troy@weareeanda.com', 'Troy Planet', '+61415310502', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(301, 'guanyeoh2009@gmail.com', 'Guan Yeoh', '60139221686', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(300, 'ash@fridaynightlabs.com', 'Ash Sharma', '5102904043', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(299, 'l.cofield@bmgpool.com', 'Lawrence Cofield', '+16043776925', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(252, 'ferwind@hotmail.com', 'Fernando Quiroga', '0034680591884', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(253, 'Admin@d2dsales.co.uk', 'Paul Thompson', '07769549026', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(269, 'ole.knutli@bitruption.com', 'Ole Andre Knutli', '+4741105954', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(268, 'nick.gallop@mail.com', 'nicholas gallop', '01163672889', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(304, 'yedige@blockchair.com', 'Yedige Davletgaliyev', '+74950000000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(307, 'jeff@lives.one', 'Jeff Chen', '+8613426276108', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(266, 'axiomax01@gmail.com', 'Alex Garnock', '+61416469080', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(264, 'citydesignlab@gmail.com', 'ken sato', '15852001919', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(240, 'ceo@tecsynt.com', 'Dmitry Stepanov', '+380634118196', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(270, 'rjadegeest@gmail.com', 'Rick de Geest', '+37127466804', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(302, 'veaydr@protonmail.com', 'Di Yan', '+4917634105101', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(303, 'kurt.vouri@guild1.com', 'Kurt Vouri', '4038602900', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(315, 'aleks@eloq.io', 'Aleksandar Dinkov', '+359883332088', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(314, 'aandrewaleks@gmail.com', 'Andrew Aleks', '85293341517', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(313, 'aandrewaleks@gmail.com', 'Andrew Alekseev', '85293341517', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(312, 'vanzuylenbert87@gmail.com', 'Bert van Zuylen', '409820440', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(311, 'vemundba@gmail.com', 'Vemund Ã…sali', '+358406604482', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(310, 'vemundba@gmail.com', 'Vemund  Ã…sali', '+358406604482', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(316, 'lzm@boquaninc.com', 'Zheming Lin', '+8615601203040', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(317, 'alex@twohop.ventures', 'Alex Fauvel', '+447525160341', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(318, 'james.belding@gmail.com', 'James Belding', '+61448878717', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(319, 'g@sdfgdfg.grt', 'dfgdsfgdf gdfg', '98734579', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(320, 'test@test.tom', 'Test Test', '84757', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(321, 'viktoria.cerena@gmail.com', 'Viktoria Cerena', '07403979747', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(322, 'viktoria.cerena@gmail.com', 'Viktoria Cerena', '07403979747', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(323, 'lise@rawpool.com', 'Lise Li', '+8619902055058', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(324, 'jan@twohop.ventures', 'Jan Smit', '0031622209303', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(325, 'Jb@bitnesis.com', 'Jose Batalla', '+61403529456', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(326, 'VeayDr@protonmail.com', 'Di  Yan', '+4917634105101', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(327, 'Jorge@JorgeRobins.co.uk', 'Jorge Robins', '07541813410', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(328, 'nguyencuong097@gmail.com', 'cuong nguyen van', '0911785989', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(329, 'dmaurusbit@gmail.com', 'Mauro Amorim', '+5562993775273', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(330, 'samsurrahman6577@gmail.com', 'samsur rahman', '01796992028', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(331, 'leaderjohn001@gmail.com', 'Kablan Jean Baptiste AMAN', '22508623720', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(332, 'jjones@vcmining.com', 'Jeffrey Jones', '14048226401', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(333, 'grego@vmssecuritycloud.com', 'Gregory Ohanessian', '5162417891', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(334, 'koonkhaw32@gmail.com', 'tanapol koonkhaw', '+66860399987', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(335, 'koonkhaw32@gmail.com', 'tanapol koonkhaw', '0860399987', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(336, 'stephan@werkswinkel.net ', 'Stephan February ', '+6586132446', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(337, 'markallison@gmail.com', 'Mark Allison', '07557355105', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(338, 'thepeterblain@yahoo.com', 'Peter Blain', '+61404270424', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(339, 'maximalistgroup@gmail.com', 'Jerry French', '16822395704', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(340, 'Hassoon1986@gmail.com', 'Hassan Sabbahi ', '+32488665510', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(341, 'jhenslee1@gmail.com', 'JOSHUA HENSLEE', '6786281507', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(342, 'REITCONSULTING@GMAIL.COM', 'JAESUN LEE', '2012109553', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(343, 'Christian.Richards@Fibrecentre.com', 'Christian Richards', '+447495600214', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(344, 'Christian@eliseustech.com', 'Christian Richards', '+447495600214', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(345, 'joel@dalais.org', 'Joel Dalais', '07918053026', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(346, 'bill@zietzke.net', 'Bill Zietzke', '2068905719', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(347, 'wormite@gmail.com', 'Lun Jiang', '+14082158507', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(348, '1watt@protonmail.ch', 'MARCO ADAMO', '+5215561922486', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(349, 'kenshishido@mac.com', 'Ken Shishido', '+81367210175', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(350, 'nicolae.pop@protonmail.ch', 'Nick Pop', '+447592235901', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(351, 'maldive@protonmail.com', 'ignat maldive', '+340642006586', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(352, 'ds@nmc.email', 'Denis Slabakov', '+79260110909', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(353, 'stevebailey@concretemedia.com.au', 'Steve Bailey', '07547354625', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(354, '1stmil.com@gmail.com', 'Gabriel Cheng', '+61416229899', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(355, 'leolopezc2@gmail.com', 'Leobardo Lopez', '6481011548', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(356, 'rogelio.reyna@pm.me', 'Rogelio Reyna', '+523311446187', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(357, 'mleblanc@nuvoo.io', 'Martin LeBlanc', '+14184553426', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(358, 'info@handcash.io', 'Alex Agut', '+34684252830', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(359, 'russ.fryman@praxms.com', 'Russell Fryman', '+15179202342', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(360, 'themorgantown@gmail.com', 'daniel morton', '3103103133', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(361, 'ryan@moneybutton.com', 'Ryan X. Charles', '+14155150210', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(362, 'wmfischer@gmail.com', 'william fischer', '07939325585', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(363, 'xorius@pm.me', 'John Hoffman', '17044438403', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(364, 'Info@bitcoin-bch.nl', 'Donald Mulders', '+31645239420', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(365, 'franciscodelinos@gmail.com', 'Francisco De Linos', '615943009', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(366, 'andrewmathes1@gmail.com', 'Andrew Mathes', '7706338697', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(367, 'haru@neonegroup.com', 'haru kamura', '7783440711', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(368, 'digitalcurrencies.coin@gmail.com', 'Rita Marcellino', '0403825100', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(369, 'Kurt@cryptotraderspro.com', 'Kurt Wuckert Jr ', '6303731469', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(370, 'oscar.peidro@gmail.com', 'Oscar Peidro', '676635901', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(371, 'george@faiacorp.com', 'George Samuels', '+61439339696', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(372, 'craigaporter@hotmail.com', 'Craig Porter', '447715103698', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(373, 'MaximalistGroup@gmail.com', 'Brandie French', '16825935401', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(374, 'hcwagneriv@gmail.com', 'Henry Wagner', '8608369151', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(375, 'k.sakurai@bnk-energy.com', 'Kazuaki Sakurai', '+77021074257', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(376, 'rextrugo2017@gmail.com', 'Rex Trugo', '00639171646572', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(377, 'hilz.note@gmail.com', 'Kyle Lee', '821098002760', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(378, 'hilz.note@gmail.com', 'Kyle Lee', '821098002760', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(379, 'brendan@coinstorage.guru', 'Brendan Lee', '+61421547744', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(380, 'eurekaoracle@gmail.com', 'Osmin Callis', '+447970497588', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(381, 'Tim@nosh.hk', 'Tim Chan', '85269083892', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(382, 'test@user.com', 'Test User', '$2y$10$HZxNGjG/naOJPcgnHwDjiuitqjg/PPSb1LpBuCsU3gaSM11gF7zs6', '2018-11-18 22:25:06', '2018-11-18 22:25:06', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'RkoaufIbk5jes6IL0paUFPdsLy37HOaa5PVvFWMqE2DxqcIHSBGRVCMUUlNq'),
(383, 'sample@user.com', 'Sample User', '$2y$10$ODbGSe.0FGN1gZZY.yQZtOK.UAZ.ldwrzoqDTlky.sQf3sz8efKle', '2018-11-18 22:27:38', '2018-11-18 22:27:38', 'active', 'member', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bcomm_events`
--
ALTER TABLE `bcomm_events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `bcomm_jobs`
--
ALTER TABLE `bcomm_jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `bcomm_pages`
--
ALTER TABLE `bcomm_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `bcomm_support`
--
ALTER TABLE `bcomm_support`
  ADD PRIMARY KEY (`ticket_id`);

--
-- Indexes for table `bcomm_tags`
--
ALTER TABLE `bcomm_tags`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bcomm_events`
--
ALTER TABLE `bcomm_events`
  MODIFY `event_id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bcomm_jobs`
--
ALTER TABLE `bcomm_jobs`
  MODIFY `job_id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bcomm_pages`
--
ALTER TABLE `bcomm_pages`
  MODIFY `page_id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bcomm_support`
--
ALTER TABLE `bcomm_support`
  MODIFY `ticket_id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bcomm_tags`
--
ALTER TABLE `bcomm_tags`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=384;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
