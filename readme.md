# bCommAssociation Laravel Build
### Documentation by Chris Brosnan - 5th November 2018

## Set up on localhost

To setup on localhost follow the steps below: 

* Install MAMP or a similar software for setting up a local LAMP stack on your machine. 
* If you do not have composer installed globally on your machine, follow the instructions to do so here: [Composer - Getting Started](). 
* Create a new laravel project in the folder for your localhost by first running a 'cd' command to the htdocs/public_html folder of your localhost. For MAMP on Mac this will be "cd /Applications/MAMP/htdocs/", while on PC this will be "cd /C/MAMP/htdocs/". Once you have run the cd command to the folder, run the following command: _composer create-project laravel/laravel _ (). More details can be found in [Laravel Documentation](https://laravel.com/docs/5.7)
* Remove the contents of the Laravel project folder created by the above step *_but not the folder itself._* 
* Create a database named 'local_laravel' through phpMyAdmin. Do not create any tables in this database. 
* In Sourcetree, check out the 'dev' branch of the project to the project folder you created. This should now be empty. 
* In phpMyAdmin, import the database tables from the included bcomm-laravel.sql file into the empty database you have just created. 
* With MAMP (or similar) running, check your project is live from the URL: "http://localhost/{foldername}/public/" or "http://localhost:8888/{foldername}/public/ (replace {foldername} with the directory name locally for the project). Any errors should be displayed. 

### If there is no output/a white screen

It is common for there to be a white screen and/or no output when cloning a Laravel project from a git repo. However, this is not a major problem and is very easily rectified. Follow the steps below: 

* This could be due to permissionings on the /storage/ folder at the root of the Laravel project. In the terminal, 'cd' to the root of the project (locally) and run the following command in the terminal: _chmod 777 -R storage_.
* "dump-autoload" composer command: _composer dump-autoload_.
* Update composer: _composer update_
* Check database configuration in .env (at root) and config/database.php.

## Laravel basics

bCommaAssociation is a professional networking service built on Laravel. 

Laravel is a PHP MVC framework that allows for rapid development of PHP applications. You can learn the basics of Laravel development through the following sources: 

* [Laravel official documentation](https://laravel.com/docs/5.7)
* [Laracasts](https://laracasts.com/)
* [Tutorials Point - Laravel](https://www.tutorialspoint.com/laravel/)

## Database

_MySQL Database:_ bcommala_db
_MySQL Username:_ bcommala_root
_MySQL Password:_ ayremedia!!2018!

### Database tables:

* bcomm_connections
* bcomm_experiences
* bcomm_messages
* migrations
* password_resets
* social_accounts
* subscribers
* users
* wp_commentdata
* wp_comments
* wp_links
* wp_options
* wp_posts
* wp_redirection_404
* wp_redirection_groups
* wp_redirection_items
* wp_redirection_logs
* wp_termmeta
* wp_terms
* wp_term_relationships
* wp_term_taxonomy
* wp_usermeta
* wp_users
* wp_yoast_seo_links
* wp_yoast_seo_meta

## Template structure

Templates are stored in the /resources/views/ directory. 

Layout templates are stored in /resources/views/layouts/ directory. 

Page templates are stored in /resources/views/pages/ directory. 

Segments (ie. header/footer/head etc) are stored in /resources/views/includes/. 

URL routing rules are set in the /routes/web.php file. 

## CMS

WordPress is installed as a headless-CMS and can be accessed via https://bcommassociation.com/wp/wp-admin/. 