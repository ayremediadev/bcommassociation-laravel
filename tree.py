import os

cwd = os.getcwd()

def list_files(currpath):
    print('\nYour current directory is: ' + currpath)
    count = 0
    for root, dirs, files in os.walk(currpath):
        count = count + 1
        for f in files:
            count = count + 1
    print('\n \nThere are ' + str(count) + ' items in this directory and all its subdirectories.')
    tree = input('Do you want to print the file tree (y/n)? ')
    while tree.lower() != 'yes' and tree.lower() != 'no' and tree.lower() != 'y' and tree.lower() != 'n':
        tree = input('Do you want to print the file tree (y/n)? ')
    if tree.lower() == 'y' or tree.lower() == 'yes':
        print('\n\n')
        for root, dirs, files in os.walk(currpath):
            level = root.replace(currpath, '').count(os.sep)
            indent = ' ' * 4 * (level)
            print('{}{}/'.format(indent, os.path.basename(root)))
            subindent = ' ' * 4 * (level + 1)
            for f in files:
                print('{}{}'.format(subindent, f))
        print('\n\n' + str(count) + ' files and folders in total')
    elif tree.lower() == 'n' or tree.lower() == 'no':
        print('\nGoodbye.')

list_files(cwd)