#!/bin/bash

# Database credentials
user="bcommass_root"
password="ayremedia!!2018!"
host="127.0.0.1"
db_name="bcommass_db"

# Other options
#backup_path="/home/bcommassociation/public_html/laravel-prod/"
date=$(date +"%d-%b-%Y")

# Set default file permissions
umask 777

# Dump database into SQL file
mysqldump --user=$user --password=$password --host=$host $db_name > $db_name-$date.sql

# Delete files older than 30 days
#find $backup_path/* -mtime +30 -exec rm {} \;