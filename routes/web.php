<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/', function () {
    return view('pages.home');
});
Route::get('/news', function () {
    return view('pages.blog-archive');
});
Route::get('/news/post', function () {
    return view('pages.blog');
});
Route::get('/user' , function () {
    return view('user-profile');
});
Route::get('/edit-profile' , function () {
    return view('edit-profile');
});
Route::get('/members' , function () {
    return view('pages.members');
});
Route::get('/subscribe-process' , function () {
    return view('pages.subscribe-process');
});
Route::get('/bch-filtered' , function () {
    return view('pages.bch-filtered');
});
Route::get('/update-avatar' , function () {
    return view('pages.update-avatar');
});
Route::get('/update-profile' , function () {
    return view('pages.update-profile');
});
Route::get('/update-interests' , function () {
    return view('pages.update-interests');
});
Route::get('/remove-interest' , function () {
    return view('pages.remove-interest');
});
Route::get('/send-connection' , function () {
    return view('pages.send-connection');
});
Route::get('/terms-service' , function () {
    return view('pages.terms-service');
});
Route::get('/privacy-policy' , function () {
    return view('pages.privacy-policy');
});
Route::get('/testemail' , function () {
    return view('pages.testemail');
});
Route::get('/mail' , function () {
    return view('pages.inbox');
});

Route::get('/messages' , function () {
    return view('pages.messages');
});

Route::get('/messages-out' , function () {
    return view('pages.messages-out');
});

Route::get('/messages-req' , function () {
    return view('pages.messages-req');
});

Route::get('/message' , function () {
    return view('pages.single-message');
});

Route::get('/new-message' , function () {
    return view('pages.new-message');
});

Route::get('/message-sent' , function () {
    return view('pages.message-sent');
});

Route::get('/confirmconnect' , function () {
    return view('pages.updateconnection');
}); 

Route::get('/newuser' , function () {
    return view('pages.newuser');
}); 

Route::get('profile', 'UserProfileController@profile');
Route::post('profile', 'UserProfileController@update_avatar');
Route::get('defaultAvatar', 'UserProfileController@defaultAvatar');
Route::post('defaultAvatar', 'UserProfileController@default_avatar');

/*** WP API Routes ***/

$jsondata = (array)json_decode(file_get_contents('https://bcommassociation.com/laravel-prod/public/wp/wp-json/wp/v2/posts/'));
$wparray = array();
foreach($jsondata as $post){
	$array = (array)$post;
	array_push($wparray, $array['slug']);
}
foreach($wparray as $wppost){
	Route::get('/news' . '/' . $wppost, function () use ($wppost) {
        return view('pages.blog');
    }); 
}

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('login/{provider}', 'Auth\SocialAccountController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\SocialAccountController@handleProviderCallback');
//Route::get('logout', 'Auth\LoginController@logout')->name('logout' );
