<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use Auth;
use Image;
use File;

// $base = URL::to('/');
// $path = str_replace("/public","",$base); 

class UserProfileController extends Controller
{
   public function profile()
    {
        $user = Auth::user();
        return view('edit-profile',compact('user',$user));
    }
	
	public function defaultAvatar()
    {
        $user = Auth::user();
        return view('edit-profile',compact('user',$user));
    }
	
     public function update_avatar(Request $request){

        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $user = Auth::user();

        $avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();

        $request->avatar->storeAs('avatars',$avatarName);
		$image = Image::make($request->avatar)->fit(300)->encode(request()->avatar->getClientOriginalExtension());
		
		$image->save(storage_path('app/avatars/').$avatarName,80);

        $user->avatar = $avatarName;
        $user->save();

        return back()
            ->with('success','You have successfully upload image.');

    }
	
	public function default_avatar(Request $request){

        /*$request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);*/

        $user = Auth::user();

        $avatarName = 'default01.jpg';

        //$request->defaultAvatar->storeAs('avatars',$avatarName);

        $user->avatar = $avatarName;
        $user->save();

        return back()
            ->with('success','You have successfully upload image.');

    }
	
}

