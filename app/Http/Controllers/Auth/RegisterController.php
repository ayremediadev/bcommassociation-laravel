<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/newuser';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
			'title' => 'required|string|max:255',
			'phone' => 'required|string|max:255',
			'location' => 'required|string|max:255',
			'position' => 'required|string|max:255',
			'company' => 'required|string|max:255',
			'tagline' => 'required|string|max:255',
			'bsv_profile' => 'required|string|max:255',
			'opportunities' => 'required|string|max:255',
			'updates' => 'required|int|max:3',
			'tandc' => 'required|int|max:3',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
	
		if(isset($data['receive-updates'])){
			$updates = $data['receive-updates'];
		} else {
			$updates = 1;
		}
		
		if(isset($data['tandc'])){
			$tandc = $data['tandc'];
		} else {
			$tandc = 1;
		}
	
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
			'title' => $data['title'],
			'phone' => $data['phone'],
			'location' => $data['location'],
			'position' => $data['position'],
			'company' => $data['company'],
			'tagline' => $data['tagline'],
			'bsv_profile' => $data['bsv_profile'],
			'opportunities' => $data['opportunities'],
			'updates' => $updates, 
			'tandc' => $tandc, 
			
        ]);
    }
}
