<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ZohoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zoho:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Zoho customers database on a daily basis with new users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
