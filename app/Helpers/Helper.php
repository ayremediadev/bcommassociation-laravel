<?php 

function url_replace(){

    $base = URL::to('/');
    echo $base . '/';

}

function storage_url($folder , $file){

	if(strpos(URL::to('/'), 'https://bcommassociation.com')){
		$base = URL::to('/') . '/laravel-prod' . Storage::url($folder);
	} else {
		$base = URL::to('/') . Storage::url($folder);
	}
	echo $base . '/' . $file;

}

function avatar($file) {
	
	if(strpos(URL::to('/'), 'https://bcommassociation.com')){
		$base = URL::to('/') . '/laravel-prod' . Storage::url('app/avatars/' . $file);
	} else {
		$base = URL::to('/') . Storage::url('app/avatars/' . $file);
	}
	echo $base; 

}

function icon($file){ 
	
	if(strpos(URL::to('/'), 'https://bcommassociation.com')){
		$base = URL::to('/') . '/laravel-prod' . Storage::url('app/img/icons/' . $file);
	} else {
		$base = URL::to('/') . Storage::url('app/img/icons/' . $file);
	}
	echo $base; 
	
} 

function img($file){ 
	
	if(strpos(URL::to('/'), 'https://bcommassociation.com')){
		$base = URL::to('/') . '/laravel-prod' . Storage::url('app/img/' . $file);
	} else {
		$base = URL::to('/') . Storage::url('app/img/' . $file);
	}
	echo $base; 
	
} 

function logo($file){ 
	
	if(strpos(URL::to('/'), 'https://bcommassociation.com')){
		$base = URL::to('/') . '/laravel-prod' . Storage::url('app/img/logos/' . $file);
	} else {
		$base = URL::to('/') . Storage::url('app/img/logos/' . $file);
	}
	echo $base; 
	
} 

class User{
	
	public function id(){ return Auth::user()->id; }
	
	public function name(){ return Auth::user()->name; }
	
	public function email(){ return Auth::user()->email; }
	
	public function position(){ return Auth::user()->position; }
	
	public function company(){ return Auth::user()->company; }
	
	public function location(){ return Auth::user()->location; }
	
	public function avatar(){ return Auth::user()->avatar; }
	
	public function tagline(){ return Auth::user()->tagline; }
	
	public function presentation(){ return Auth::user()->presentation; }
	
	public function bch_profile(){ return Auth::user()->bch_profile; }
	
	public function interests(){ return Auth::user()->interests; }
	
	public function opportunities(){ return Auth::user()->opportunities; }
	
	public function experiences(){ return Auth::user()->experiences; }
	
	public function connections(){ return Auth::user()->connections; }
	
	public function phone(){ return Auth::user()->phone; }
	
	public function messages(){ return Auth::user()->messages; }
	
	public function unread_messages(){ return Auth::user()->unread_messages; }

}

?>