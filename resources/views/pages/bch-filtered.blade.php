<?php 
//echo 'test';
 //$base = URL::to('/');
  //$path = str_replace("/public","",$base); 

function imgs($file) {
  //$base = URL::to('/');
  $path = 'https://bcommassociation.com';
  $img = $path . Storage::url('app/img/' . $file);
  echo $img; 
} 

$id = Auth::user()->id; 
$name = Auth::user()->name;
$position = Auth::user()->position;
$company = Auth::user()->company;
$location = Auth::user()->location;
$avatar = Auth::user()->avatar;
$tagline = Auth::user()->tagline; 
$presentation = Auth::user()->presentation; 
$bchprofile = Auth::user()->bch_profile; 
$interests = Auth::user()->interests; 
$opportunities = Auth::user()->opportunities; 
$experiences = Auth::user()->experiences; 
$connections = Auth::user()->connections; 
$phone = Auth::user()->phone;


$users = DB::table('users')
                        ->orderBy('id', 'desc')
                        ->take(60)
                        ->where('id', '!=', $id)
                        ->get(); 

if(isset($_GET['profile'])){
	$bchprofile = $_GET['profile'];
} else {
	$bchprofile = array('allprofiles'); 
}

if(isset($_GET['city'])){
	$cities = $_GET['city'];
} else {
	$cities = array('allcities');  
}
if(isset($_GET['interest'])){
	$interests = $_GET['interest'];
} else {
	$interests = array('allinterests');  
}

// $citiesString = '';
// 		foreach($cities as $city){
// 			$citiesString .= $city . ', ';
// 		}

// $bchproString = '';
// 		foreach($bchprofile as $profile){
// 			$bchproString = $profile;
// 		}

// $intString = '';
// 		foreach($interests as $interest){
// 			$intString = $interest;
// 		}

?>
<head>
    <base target="_parent">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Get css links from parent page -->
    <script>
    window.onload = function() {
        Array.prototype.forEach.call(window.parent.document.querySelectorAll("link[rel=stylesheet]"), function(link) {
            var newLink = document.createElement("link");
            newLink.rel  = link.rel;
            newLink.href = link.href;
            document.head.appendChild(newLink);
        });
    };
    </script>
</head>

<div id="app">
<main class="py-4" style="margin-top: 0;">
<main role="main" style="margin-top: 20px">
	
	<?php //print_r($bchprofile); ?>
	
<section class="other_bcommmembers">
    <div class="container py-4">
        <div class="row">
            <div class="col-md-9 offset-md-3">
                <!-- START BCOMM ASSOCIATE MEMBERS -->
                <div class="row align-items-center justify-content-center box-header mb-5 text-center">
                    <div class="col"><h3 class="my-0">bComm Association members</h3></div>
                </div>
                <div class="row align-items-center justify-content-center box-header mb-3 text-center">
<?php 
foreach($users as $user){
    if(in_array('allinterests', $interests) && in_array('allcities', $cities) && in_array('allprofiles', $bchprofile)){ ?>

        
            <div class="col-6 col-md-4">
                <div class="card user-card text-center mb-4 shadow-md margin-top-50 filterDiv profile_<?php echo $user->bch_profile;?>">
                    <div class="user-pic pic-lg mb-1">
                        <img src="<?php avatar($user->avatar); ?>">
                    </div>
                    <div class="card-body">
                        <div class="ls-1 text-uppercase font-weight-bold mb-2"><?php echo $user->name; ?></div>
                        <div class="mb-2"><?php if($user->position){ echo $user->position; ?> at <?php echo $user->company; } else { echo '<br/>'; } ?><br/>
							<?php if($user->location) { echo $user->location; } ?></div>
                        <div class="dropdown-divider mb-4"></div>
                        <!--<div class="font-weight-light mb-4"><?php //echo $user->tagline; ?></div>-->
                        <button type="button" class="btn btn-outline-gradient-lg-green" onclick="javascript:top.window.location.href='https://bcommassociation.com/user?id=<?php echo $user->id; ?>'" formtarget="_blank"><span>See public profile</span></button>
                    </div>
                </div>
            </div>
        
    <?php 

    } else{
        foreach ($bchprofile as $profile) {
            //print_r ($profile);
            foreach ($cities as $city) {
                foreach ($interests as $interest) {
                // if(strpos(strtolower($user->bch_profile), strtolower($profile)) !== false || strpos(strtolower($profile), strtolower($user->position)) !== false || strpos($user->location, $city) !==false || mb_strpos($user->interests,strtolower($intString)) !== false) 
                    if(strpos(strtolower($user->bch_profile), strtolower($profile)) !== false || strpos(strtolower($user->position), strtolower($profile)) !== false || strpos($user->location, $city) !==false || strpos($user->interests,strtolower($interest)) !== false){ ?>

                        <div class="col-6 col-md-4">
                            <div class="card user-card text-center mb-4 shadow-md margin-top-50 filterDiv profile_<?php echo $user->bch_profile;?>">
                                <div class="user-pic pic-lg mb-1">
                                    <img src="<?php avatar($user->avatar); ?>">
                                </div>
                                <div class="card-body">
                                    <div class="ls-1 text-uppercase font-weight-bold mb-2"><?php echo $user->name; ?></div>
                                    <div class="mb-2"><?php if($user->position){ echo $user->position; ?> at <?php echo $user->company; } else { echo '<br/>'; } ?><br/>
										<?php if($user->location) { echo $user->location; } ?></div>
                                    <div class="dropdown-divider mb-4"></div>
                                        <!--<div class="font-weight-light mb-4"><?php //echo $user->tagline; ?></div>-->
                                    <button type="button" class="btn btn-outline-gradient-lg-green" onclick="javascript:top.window.location.href='https://bcommassociation.com/user?id=<?php echo $user->id; ?>'" formtarget="_blank"><span>See public profile</span></button>
                                </div>
                            </div>
                        </div>
<?php               }
                }
             }
         }
    }
}   
?>
                <!-- END BCOMM ASSOCIATE MEMBERS -->
                </div>
            </div>
        </div>
    </div>
</section>
</main>
</main>
</div>
