<?php 
        
$id = Auth::user()->id;
$name = Auth::user()->name;
$email = Auth::user()->email;
$updates = Auth::user()->updates;
$tandc = Auth::user()->tandc;

$space = strpos($name, ' ');
$fname = substr($name, 0, $space);
$lname = substr($name, ($space + 1));

DB::table('subscribers')->insertGetId(
    ['email' => $email,  
    'fullname' => $name, 
    'phone' => NULL, 
    'message' => NULL, 
    'join_assoc' => NULL, 
    'subscribe' => 'Yes', 
    'categories' => NULL, 
    'company' => NULL, 
    'date' => date('Y-m-d')]
);  

?>

<html>
<head>

    <!-- JQUERY -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
    <script>
        $(document).ready(function(){
            $('#zohocrmform').submit();
        });
    </script>
	
	<script>
        $(document).ready(function(){
			var delay = 1000; 
			setTimeout(function(){ window.location = 'https://bcommassociation.com/home'; }, delay);
        });
    </script>

</head>
    <body>
        <!-- Note :
   - You can modify the font style and form style to suit your website. 
   - Code lines with comments ���Do not remove this code���  are required for the form to work properly, make sure that you do not remove these lines of code. 
   - The Mandatory check script can modified as to suit your business needs. 
   - It is important that you test the modified form before going live.-->
<div id='crmWebToEntityForm' style='width:600px;margin:auto;'>
   <META HTTP-EQUIV ='content-type' CONTENT='text/html;charset=UTF-8'>
   <form id="zohocrmform" style="display: none;" action='https://crm.zoho.com/crm/WebToContactForm' name=WebToContacts3647163000000205003 method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory3647163000000205003()' accept-charset='UTF-8'>

	 <!-- Do not remove this code. -->
	<input type='text' style='display:none;' name='xnQsjsdp' value='1c04e6c71bd450b802299363ae5f60107fc55b7d5c48dcc55e21a058102d723c'/>
	<input type='hidden' name='zc_gad' id='zc_gad' value=''/>
	<input type='text' style='display:none;' name='xmIwtLD' value='740bdc1b7bd4a5d900ff32d201a7b110cc73b0b7ff7604cdfd2f7c717823bc79'/>
	<input type='text' style='display:none;'  name='actionType' value='Q29udGFjdHM='/>

	<input type='text' style='display:none;' name='returnURL' value='https://bcommassociation.com/home' /> 
	 <!-- Do not remove this code. -->
	<style>
		tr , td { 
			padding:6px;
			border-spacing:0px;
			border-width:0px;
			}
	</style>
	<table style='width:600px;background-color:white;color:black'>

	<tr><td colspan='2' style='text-align:left;color:black;font-family:Arial;font-size:14px;'><strong>bcomm_subscribers</strong></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>First Name</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='40' value='<?php echo $fname; ?>' maxlength='80' name='First Name' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Last Name<span style='color:red;'>*</span></td><td style='width:250px;' ><input type='text' style='width:250px;' value='<?php echo $lname; ?>' maxlength='80' name='Last Name' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Email</td><td style='width:250px;' ><input type='text' style='width:250px;'  value='<?php echo $email; ?>' maxlength='80' maxlength='100' name='Email' /></td></tr>

	<?php if($updates == 2){ ?>
	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>GDPR consent </td><td style='width:250px;' ><input type='checkbox'  name='CONTACTCF104' checked/></td></tr>
	<?php } else { ?>
	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>GDPR consent </td><td style='width:250px;' ><input type='checkbox'  name='CONTACTCF104' /></td></tr>
	<?php } ?>

	<?php if($tandc == 2){ ?>
	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>T&amp;Cs consent </td><td style='width:250px;' ><input type='checkbox'  name='CONTACTCF110' checked /></td></tr>
	<?php } else { ?>
	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>T&amp;Cs consent </td><td style='width:250px;' ><input type='checkbox'  name='CONTACTCF110' /></td></tr>
	<?php } ?>
	
	<tr><td colspan='2' style='text-align:center; padding-top:15px;font-size:12px;'>
		<input style='font-size:12px;color:black'  type='submit' value='Submit' />
		<input  type='reset' style='font-size:12px;color:black' value='Reset' />
	    </td>
	</tr>
   </table>
	<script>
 	  var mndFileds=new Array('Last Name');
 	  var fldLangVal=new Array('Last Name');
		var name='';
		var email='';

 	  function checkMandatory3647163000000205003() {
		for(i=0;i<mndFileds.length;i++) {
		  var fieldObj=document.forms['WebToContacts3647163000000205003'][mndFileds[i]];
		  if(fieldObj) {
			if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
			 if(fieldObj.type =='file')
				{ 
				 alert('Please select a file to upload.'); 
				 fieldObj.focus(); 
				 return false;
				} 
			alert(fldLangVal[i] +' cannot be empty'); 
   	   	  	  fieldObj.focus();
   	   	  	  return false;
			}  else if(fieldObj.nodeName=='SELECT') {
  	   	   	 if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
				alert(fldLangVal[i] +' cannot be none'); 
				fieldObj.focus();
				return false;
			   }
			} else if(fieldObj.type =='checkbox'){
 	 	 	 if(fieldObj.checked == false){
				alert('Please accept  '+fldLangVal[i]);
				fieldObj.focus();
				return false;
			   } 
			 } 
			 try {
			     if(fieldObj.name == 'Last Name') {
				name = fieldObj.value;
 	 	 	    }
			} catch (e) {}
		    }
		}
	     }
	   
</script>
	</form>
</div>

</body>