<?php 
$baseURL = URL::to('/') . '/'; 
$csspath = str_replace("/public","",$baseURL);

function imgs($file){
    $base = URL::to('/');
    //$path = str_replace("/public","",$base); 
    $img = 'https://bcommassociation.com/laravel-prod' . Storage::url('app/img/' . $file);
    echo $img; 
} 

Route::get('user/profile', ['as' => 'my-profile', 'uses' => 'UserController@profile']);

// This fetches user data based on queried ID
$logged_id = Auth::user()->id;
$id = $_GET['id']; 
$user = DB::table('users')->where('id', $id)->first();
$name = $user->name;
$position = $user->position;
$company = $user->company;
$location = $user->location;
$avatar = $user->avatar;
$tagline = $user->tagline; 
$presentation = $user->presentation; 
$bchprofile = $user->bch_profile; 
$interests = $user->interests; 
$opportunities = $user->opportunities; 
$experiences = $user->experiences; 
$connections = $user->connections; 
$phone = $user->phone; 

//echo Auth::user()->connections;
?>

<main role="main" id="profile" style="margin-top: 20px">

  <section class="profile">
    <div class="container py-5">
      <div class="row justify-content-center">
        <div class="col-md-10">

          <div class="card user-card mb-4 shadow-md margin-top-50 pb-5">

            <div class="container">
              <div class="row">

                <div class="col-md-3" style="text-align: left;">
                  <a href="https://bcommassociation.com/edit-profile" class="text-link"><i class="fas fa-long-arrow-alt-left"></i> Edit Profile</a>
                </div>

                <div class="col-md-6 text-center ">

                  <div class="user-pic pic-lg mb-1">
                    <img src="<?php avatar($avatar); ?>">
                  </div>
                  <div class="card-body">
                    <div class="ls-1 text-uppercase font-weight-bold mb-2"><?php echo $name; ?></div>
                    <div class="mb-2"><?php echo $position; ?></div>
                    <div class="font-weight-light mb-4"><?php echo $location; ?></div>
                    <div class="dropdown-divider mb-4"></div>
                    <div class="font-weight-light mb-4"><?php echo $tagline; ?></div>
                  </div>

                </div>

                <div class="col-md-3 text-right">
                  <?php 
                      $bcomm_connections = DB::table('bcomm_connections')
                        ->where('user_id_1', '=', $logged_id, 'OR', 'user_id_2', '=', $logged_id)
                        ->get();
                      if($logged_id != $id){ 
                          if(Auth::user()->connections == 0){ 
                    ?>
                      <form class="container" id="sendConnect" style=" font-size: .8em;" method="get" target="sendconnection" action="https://bcommassociation.com/send-connection">
                          <input type="hidden" value="<?php echo Auth::user()->id; ?>" name="user1" />
                          <input type="hidden" value="<?php echo $id; ?>" name="user2" />
                          <button type="submit" class="btn btn-outline-gradient-lg-green mt-3" style="margin-top: 16px"><span><img src="<?php echo $csspath; ?>template/resources/img/bcomm-icon.svg" class="btn-icon"> Connect1</span></button>
                      </form>
                      <iframe src="https://bcommassociation.com/send-connection" id="sendconnection" name="sendconnection" style="display: none;"></iframe>
                  <?php } else{ 

                        $array = array();
                        foreach ($bcomm_connections as $bcomm_connection) {
                         if( $bcomm_connection->user_id_1 != $logged_id){
                            
                            array_push($array, $bcomm_connection->user_id_1);
                          }
                          if($bcomm_connection->user_id_2 != $logged_id ){
                            array_push($array, $bcomm_connection->user_id_2);
                          }
                        }

                       if(!in_array($id, $array)){ ?>
                          <form method="get" id="connect-mbs" target="sentinvite" action="https://bcommassociation.com/send-connection">
                            <input type="hidden" name="connection" value="1">
                            <input type="hidden" name="user1" value="<?php echo Auth::user()->id; ?>">
                            <input type="hidden" name="user2" value="<?php echo $id;?>">
                            <button type="submit" class="btn btn-outline-gradient-lg-green" value="submit" onClick="window.location.reload()" style="margin-top: 16px"><span><img src="<?php echo $csspath; ?>/template/resources/img/bcomm-icon.svg" class="btn-icon"> Connect2</span></button>
                          </form> 
                  <?php }else{  foreach ($bcomm_connections as $bcomm_connection) {
                        switch ($bcomm_connection->connection) {
                        case '1':
                          if($bcomm_connection->user_id_1 == $logged_id && $bcomm_connection->user_id_2 == $id){ ?>
                            <button type="button" class="btn btn-outline-gradient-lg-green" disabled style="margin-top: 16px"><span><img src="<?php echo $csspath; ?>/template/resources/img/bcomm-icon.svg" class="btn-icon"> Invite sent</span></button>
                    <?php }elseif ($bcomm_connection->user_id_2 == $logged_id && $id == $bcomm_connection->user_id_1) { ?>
                            <button type="button" class="btn btn-outline-gradient-lg-green" disabled style="margin-top: 16px"><span><img src="<?php echo $csspath; ?>/template/resources/img/bcomm-icon.svg" class="btn-icon"> Accept request</span></button>
                    <?php }break;
                        case '2':
                          if($bcomm_connection->user_id_1 == $logged_id && $bcomm_connection->user_id_2 == $id){ ?>
                            <button type="button" class="btn btn-outline-gradient-lg-green" disabled style="margin-top: 16px"><span><img src="<?php echo $csspath; ?>/template/resources/img/bcomm-icon.svg" class="btn-icon"> Connected</span></button>
                    <?php }break;

                      }
                  }
                 }
                }
                }?>
                </div>

              </div>
            </div>
            <iframe src="https://bcommassociation.com/send-connection" name="sentinvite" style="display: none;"></iframe>
            <!--<div class="container">
              <div class="row justify-content-center">
                <div class="col-md-6">
                  <div class="d-flex justify-content-between align-items-center mb-4">
                    <div class="font-weight-bold">Experiences</div>
                  </div>

                  <div class="row align-items-center justify-content-between">
                    <div class="col-auto my-2">
                      <div class="company-logo shadow-md">
                        <img src="<?php //echo $csspath; ?>template/resources/img/6SpDPegx_400x400.jpg" class="img-fluid">
                      </div>
                    </div>
                    <div class="col">
                      <div class="experience_position"><strong>Founder &amp; Head of Design</strong></div>
                      <div class="experience_company">Mad Studio</div>
                      <div class="experience_location_tenure">Paris, France | 8 years 3 months</div>
                    </div>

                    <div class="col-12 my-2">
                      <p class="mb-0 font-weight-light">Proin mattis facilisis velit. Fusce lacinia lorem sed magna dignissim, vel egestas neque finibus. Suspendisse posuere tincidunt libero. Curabitur nec nunc ligula. Sed nec est velit. Aliquam sed mauris ex.</p>
                    </div>
                  </div>

                  <div class="dropdown-divider mt-4 mb-4"></div>

                  <div class="row align-items-center justify-content-between">
                    <div class="col-auto my-2">
                      <div class="company-logo shadow-md">
                        <img src="<?php //echo $csspath; ?>template/resources/img/cg.png" class="img-fluid">
                      </div>
                    </div>
                    <div class="col">
                      <div class="experience_position"><strong>Designer</strong></div>
                      <div class="experience_company">CoinGeek Group</div>
                      <div class="experience_location_tenure">London, UK | 6 months</div>
                    </div>

                    <div class="col-12 my-2">
                      <p class="mb-0 font-weight-light">Proin mattis facilisis velit. Fusce lacinia lorem sed magna dignissim, vel egestas neque finibus. Suspendisse posuere tincidunt libero. Curabitur nec nunc ligula. Sed nec est velit. Aliquam sed mauris ex.</p>
                    </div>
                  </div>

                </div>
                <div class="col-md-3">
                  <div class="d-flex justify-content-between align-items-center mb-4">
                    <div class="font-weight-bold">Interests</div>
                  </div>

                  <div>
                    <?php $intArray = explode(', ', $interests); 
                        foreach ($intArray as $int) {
                            if(strlen($int) > 1){
                                echo "<button type='button' class='btn btn-outline-sm tags mb-1'><span>" . $int . "</span></button> ";
                            }
                        } ?>
                  </div>

                </div>

              </div>
            </div>-->

          </div>

        </div>

      </div>
    </div>

    <div class="container-fluid fixed-top bg-charcoal bg-profile"></div>
  </section>

</main>