@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header"><a class="btn-linkedin" href="{{ url('login/linkedin') }}">Sign up with linkedin</a></div>

                <div class="card-body"> 

                    <form method="POST" action="{{ route('register') }}" id="regForm" onsubmit="if(document.getElementById('agree').checked) { return true; } else { alert('Please indicate that you have read and agree to the Terms and Conditions'); return false; }">
                        @csrf

                        <div id="page1" class="step">

                                <div class="form-group row">
                                     <label for="personalinfo_Title" class="col-md-12 col-form-label">Title</label>
                                      <select class="form-control form-control-sm" id="personalinfo_Title" name="title">
                                        <?php $titles = array('Mr', 'Ms', 'Mrs', 'Miss', 'Dr', 'Prof'); 
                                        foreach($titles as $t){ ?>
                                          <option value="<?php echo $t . ' '; ?>"><?php echo ' ' . $t; ?>.</option>
                                        <?php } ?>
                                      </select>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-12 col-form-label">Full Name</label>

                                    <div class="col-md-12">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-12">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-12 col-form-label">{{ __('Password') }}</label>

                                    <div class="col-md-12">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-12 col-form-label">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-12">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button id="gotonextpage2" class="btn btn-primary" type="button">
                                        Continue
                                    </button>
                                </div>
                            </div>

                        </div>

                        <div id="page2" class="step">

                                <input type="hidden" name="avatar" value="default01.jpg" />
                                <input type="hidden" name="experiences" value=" " />
                                <input type="hidden" name="updates" value="1" />

                                <div class="form-group row">
                                    <label for="phone" class="col-md-12 col-form-label">Phone number</label>

                                    <div class="col-md-12">
                                        <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="location" class="col-md-12 col-form-label">Location</label>

                                    <div class="col-md-12">
                                        <input id="location" type="text" class="form-control" name="location" value="{{ old('location') }}" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="position" class="col-md-12 col-form-label">Position</label>

                                    <div class="col-md-12">
                                        <input id="position" type="text" class="form-control" name="position" value="{{ old('position') }}" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="company" class="col-md-12 col-form-label">Company</label>

                                    <div class="col-md-12">
                                        <input id="company" type="text" class="form-control" name="company" value="{{ old('company') }}" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="tagline" class="col-md-12 col-form-label">Bio</label>

                                    <div class="col-md-12">
                                        <textarea type="text" name="tagline" class="form-control form-control-sm" id="tagline" rows="5" name="tagline" placeholder="Short introduction to you"></textarea>    
                                    </div>
                                </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button id="gotonextpage3" class="btn btn-primary" type="button">
                                        Continue
                                    </button>
                                </div>
                            </div>

                        </div>

                        <div id="page3" class="step">

                                <div class="form-group row">
                                    <label for="bch_profile" class="col-md-12 col-form-label">BSV Profile</label>

                                    <div class="col-md-12">
                                        <!--<input id="bch_profile" type="text" class="form-control" name="bch_profile" value="{{ old('bch_profile') }}" placeholder="eg. Merchant, Miner, Investor etc..." >-->
										
										<select id="bch_profile" name="bsv_profile" style="width: 100%; padding: .7em;">
											<option value="none" disabled selected>Select a BSV profile</option>
											<option value="merchant">Merchant</options>
											<option value="miner">Miner</options>
											<option value="hedge fund/exchange">Hedge Fund/Exchange</options>
											<option value="media">Media</options>
											<option value="developer">Developer</options>
											<option value="venture capitalist">Venture Capitalist</options>
										</select>
										
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="interests" class="col-md-12 col-form-label">Interests</label>

                                    <div class="col-md-12" style="color: #fff; text-align: left;">
                                        <!--<textarea type="text" name="interests" class="form-control form-control-sm" id="interests" rows="5" name="interests" placeholder="eg. Fintech, Development, Mining etc"></textarea>-->
										
											<div class="col-md-6">
												
												<input type="checkbox" name="interests[]" value="Tech" id="interest_Tech"/>
												<label class="form-check-label" for="interest_Tech">Tech</label><br/>
											
												<input type="checkbox" name="interests[]" value="Fintech" id="interest_Fintech"/>
												<label class="form-check-label" for="interest_Fintech">Fintech</label><br/>								
												
												<input type="checkbox" name="interests[]" value="Bitcoin" id="interest_BTC"/>
												<label class="form-check-label" for="interest_BTC">Bitcoin</label>
											
											</div>
						
											<div class="col-md-6">
												
												<input type="checkbox" name="interests[]" value="BitcoinSV" id="interest_BCH"/>
												<label class="form-check-label" for="interest_BCH">Bitcoin SV</label><br/>
											
												<input type="checkbox" name="interests[]" value="Blockchain" id="interest_Blockchain"/>
												<label class="form-check-label" for="interest_Blockchain">Blockchain</label><br/>
											
												<input type="checkbox" name="interests[]" value="Mining" id="interest_Mining"/>
												<label class="form-check-label" for="interest_Mining">Mining</label>
										
											</div>
										
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="opportunities" class="col-md-12 col-form-label">Opportunities</label>

                                    <div class="col-md-12">
                                        <textarea type="text" name="opportunities" class="form-control form-control-sm" id="opportunities" rows="5" name="opportunities" placeholder="eg. I am looking for a web developer etc..."></textarea>    
                                    </div>
                                </div>
								
								<div class="form-group row mb-0">
                            <div class="col-md-12" style="margin-bottom: 3em;"> 
								<br/><br/>
                                <input type="checkbox" name="tandc" value="2" id="agree" /> <span style="color: #fff;">I agree to the bComm Association <a href="<?php  echo URL::to('/'). '/terms-service'?>" style="color: #13CE66; text-transform: none;">Terms & Conditions</a></span>
                                <br >
                                <input type="checkbox" name="receive-updates" value="2" id="receive-updates" /> <span style="color: #fff;">I would like to sign up to receive updates from the bComm Association. See the <a href="<?php  echo URL::to('/'). '/privacy-policy'?>" style="color: #13CE66; text-transform: none;">Privacy Policy</a></span>
								<br/>
                            </div>
                        </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary" id="nextBtn">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function(){
        $('#gotonextpage2').click(function(){
            if($('#page1').css('display') == 'block'){
                $('#page1').css('display', 'none'); 
                $('#page2').css('display', 'block'); 
                $('#page3').css('display', 'none'); 
            }
        });
        $('#gotonextpage3').click(function(){ 
            if($('#page2').css('display') == 'block'){
                $('#page1').css('display', 'none'); 
                $('#page2').css('display', 'none'); 
                $('#page3').css('display', 'block'); 
            }
        });
    });
</script>
<script type="text/javascript">
        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab

        function showTab(n) {
        // This function will display the specified tab of the form ...
        var x = document.getElementsByClassName("step");
        x[n].style.display = "block";
        // ... and fix the Previous/Next buttons:
        // if (n == 0) {
        //   document.getElementById("prevBtn").style.display = "none";
        // } else {
        //   document.getElementById("prevBtn").style.display = "inline";
        // }
        // if (n == (x.length - 1)) {
        //     document.getElementById("nextBtn").innerHTML = "Register";
        // } else {
        //     document.getElementById("gotonextpage").innerHTML = "Continue";
        // }
        // ... and run a function that displays the correct step indicator:
        //fixStepIndicator(n)
        }
</script>
@endsection
