<section id="header">
	<div class="top-nav" style="text-align: right; padding: 2rem 10px 0;"><a href="https://twitter.com/bCommworld?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @bCommworld</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>
    <nav class="navbar navbar-expand-md navbar-dark bg-charcoal just-desk">
        <div class="navbar-inner row">
            <div id="logoDiv" class="col-md-3 col-lg-3 col-xl-2 col-sm-12 col-xs-12">
                <?php $homeUrl = URL::to('/') . Storage::url('app/img/bcomm-rgb-logo-light.svg'); 
                ?>
                <a id="logo" href="<?php echo URL::to('/'); ?>">            
                    <?php $logoUrl = str_replace("/public", "/laravel-prod", $homeUrl); ?>
                    <img id="logoimage" src="https://bcommassociation.com/laravel-prod/storage/app/img/bcomm-rgb-logo-light.svg" />
                </a>
            </div>
            <div id="navBar" class=" col-md-6 col-lg-6 col-xl-8 col-sm-12 col-xs-12">
                <ul id="navigation" class="nav">
                    <?php $baseUrl = 'https://bcommassociation.com'; ?>
                    <li class="nav-item active"><a href="<?php echo $baseUrl; ?>/#community">Community</a></li>
                    <li class="nav-item"><a href="<?php echo $baseUrl; ?>/#about">Membership</a></li>
                    <li class="nav-item"><a href="<?php echo $baseUrl; ?>/#founders">About</a></li>
					<li class="nav-item"><a href="<?php echo $baseUrl; ?>/news">News</a></li>
                </ul>
            </div>
			
			<?php //use Auth; ?>
				
			@if(Auth::guest()) 
			
				<div id="loginNavBar" class="col-md-3 col-lg-3 col-xl-2 col-sm-12 col-xs-12">
					<ul id="navigation" class="nav">
						<li class="accountLink"><a href="<?php echo $baseUrl; ?>/login">Login</a></li>
						<li class="accountLink"><a href="<?php echo $baseUrl; ?>/register">Apply</a></li>
					</ul>
				</div>

			@else
			
				<div id="loginNavBar" class="col-md-3 col-lg-3 col-xl-2 col-sm-12 col-xs-12">
					<ul id="navigation" class="nav">
						<li class="accountLink"><a href="<?php echo $baseUrl; ?>/home">My Account</a></li>
						<li class="accountLink"><a href="<?php echo $baseUrl; ?>/logout">Logout</a></li>
					</ul>
				</div>
				
			@endif
			
        </div>
    </nav>
    <nav class="navbar navbar-expand-md navbar-dark bg-charcoal just-mob">
    	 <div class="navbar-inner row"> 
    	 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#test" aria-expanded="false"> <span class="sr-only">Toggle navigation</span>
			         <span class="icon-bar" style="background: white"></span>
			         <span class="icon-bar" style="background: white"></span>
			         <span class="icon-bar" style="background: white"></span>
		 </button> 				
		 <button type="button" class="navbar-toggle users" data-toggle="collapse" data-target="#test2" aria-expanded="false"> <i class="fas fa-user" style="color: #fff"></i>
		 </button> 
            <div id="logoDiv">
                <?php $logoUrl = URL::to('/') . '/laravel-prod' . Storage::url('app/img/bcomm-rgb-logo-light.svg'); ?>
                <a id="logo" href="<?php echo URL::to('/'); ?>">            
                    <?php $logoUrl = str_replace("/public", "", $homeUrl); ?>
                    <img id="logoimage" src="https://bcommassociation.com/laravel-prod/storage/app/img/bcomm-rgb-logo-light.svg" />
                </a>
            </div>
            <div id="navBar" class="">
		  		<div class="collapse navbar-collapse navbar-toggleable-xs" id="test">
	                <ul id="navigation" class="nav-list nav navbar-nav">
	                    <?php $baseUrl = 'https://bcommassociation.com'; ?>
	                    <li class="nav-item active"><a href="<?php echo $baseUrl; ?>/#community">Community</a></li>
	                    <li class="nav-item"><a href="<?php echo $baseUrl; ?>/#about">Membership</a></li>
	                    <li class="nav-item"><a href="<?php echo $baseUrl; ?>/#founders">About</a></li>
						<li class="nav-item"><a href="<?php echo $baseUrl; ?>/news">News</a></li>
	                </ul>
            	</div>
            </div>
            
			@if(Auth::guest()) 
			
            <div id="loginNavBar">
            	<div class="collapse navbar-collapse navbar-toggleable-xs" id="test2">
	                <ul id="navigation" class="nav nav-list nav navbar-nav">
	                    <li class="accountLink"><a href="<?php echo $baseUrl; ?>/login">Login</a></li>
	                    <li class="accountLink"><a href="<?php echo $baseUrl; ?>/register">Apply</a></li>
	                </ul>
	             </div>
            </div
			
			@else
			
			<div id="loginNavBar">
            	<div class="collapse navbar-collapse navbar-toggleable-xs" id="test2">
	                <ul id="navigation" class="nav nav-list nav navbar-nav">
	                    <li class="accountLink"><a href="<?php echo $baseUrl; ?>/home">My Account</a></li>
	                    <li class="accountLink"><a href="<?php echo $baseUrl; ?>/logout">Logout</a></li>
	                </ul>
	             </div>
            </div
			
			@endif
			
        </div>

    </nav>
</section>