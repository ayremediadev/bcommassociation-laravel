<?php $baseURL = URL::to('/') . '/'; 
      $csspath = str_replace("/public/","/laravel-prod/",$baseURL); ?>
<section id="footer">
  <footer class="text-white bg-charcoal text-uppercase font-weight-light mt-5">

      <div class="container">
        <div class="row">
          <div class="squares"><i class="fas fa-square-full sq-1"></i><i class="fas fa-square-full sq-2"></i></div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <ul class="nav d-flex justify-content-around mt-4">
              <li class=""><a href="https://bcommassociation.com/terms-service">
				  Terms of Service</a></li>
              <li class=""><a href="https://bcommassociation.com/privacy-policy">
				  Privacy Policy</a></li>
              <!--<li class="">Dumque</li>-->
            </ul>
          </div>
          <div class="col-md-2 d-block text-center">
            <a href="https://bcommassociation.com/" class="navbar-brand">
              <img src="<?php url_replace(); ?>resources-templates/img/bcomm-rgb-logo-light.svg">
            </a>
          </div>
          <div class="col-md-5">
            <ul class="nav d-flex justify-content-around mt-4">
              <li class=""><a href="https://twitter.com/bCommworld?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @bCommworld</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <!-- <div id="cookieConsent">
        <div id="closeCookieConsent">x</div>
        This website is using cookies. <a href="<?php //echo $csspath . 'laravel-prod' . Storage::url('app/bComm_Terms_and_Conditions.pdf'); ?>" target="_blank">Read More</a>. <a class="cookieConsentOK">I agree</a>
    </div>
    <script>
      $(document).ready(function(){   
      setTimeout(function () {
          $("#cookieConsent").fadeIn(200);
       }, 4000);
      $("#closeCookieConsent, .cookieConsentOK").click(function() {
          $("#cookieConsent").fadeOut(200);
      }); 
      });
    </script>  -->

    <script src="<?php echo $csspath;?>laravel-prod/resources-templates/js/app-custom.js"></script>
</section>