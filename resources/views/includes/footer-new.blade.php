<section id="footer">
    <br/>
    <div class="navbar">
        <div class="navbar-inner row">
            <div class="col-md-5 col-lg-5 col-xl-5 col-sm-0 col-xs-0">
            </div>
            <div id="logoDiv" class="col-md-2 col-lg-2 col-xl-2 col-sm-12 col-xs-12">
                <div id="copyright text-right" style="text-align: center;">
                    <a id="logo" href="/laravel/public/busdev/">         
                        <?php $baseURL = URL::to('/');
                        $imgURL = str_replace("public","",$baseURL); ?>
                        <?php $LogoUrl = $imgURL . Storage::url('app/img/bcomm-rgb-logo-light.svg'); ?>
                        <img id="logoimage" src="<?php echo $LogoUrl; ?>" style="width: 75%;"/>
                    </a>
                    <!--© Copyright <?php //echo date("Y"); ?> bCommAssociation. All Rights Reserved. -->
                </div>
            </div>
            <div class="col-md-5 col-lg-5 col-xl-5 col-sm-0 col-xs-0">
            </div>
        </div>
    </div>
    <br/>
</section>