<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="description" content="The bComm Association is committed to making Bitcoin Satoshi Vision (BSV) become the new peer-to-peer electronic cash that is used globally for bCommerce.">
<link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">

<!--<meta name="description" content="">
<meta name="author" content="Scotch">--->

<title>bCommAssociation</title>

<!-- JQUERY -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

<!-- BOOTSTRAP -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- VUE.JS -->
<!-- development version, includes helpful console warnings -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<!-- production version, optimized for size and speed -->
<!-- <script src="https://cdn.jsdelivr.net/npm/vue"></script> -->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,700" rel="stylesheet">

<?php $baseURL = URL::to('/') . '/'; 
$csspath = str_replace("/public","",$baseURL); ?>

<link rel="stylesheet" href="https://bcommassociation.com/laravel-prod/resources-templates/css/circle.css">
<link rel="stylesheet" href="https://bcommassociation.com/laravel-prod/resources-templates/css/template-style.css">


<!-- MONTSERRAT FONT INCLUSION -->
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 

<style>
body {
    font-family: 'Montserrat', sans-serif !important; 
}
</style>

<?php //$cssURL = $baseURL . 'style.css'; ?>
<!-- DEFAULT STYLESHEET -->
<link rel="stylesheet" type="text/css" href="https://bcommassociation.com/laravel-prod/style.css<?php //echo $cssURL; ?>"/>

<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#364654"
    },
    "button": {
      "background": "rgba(48,202,175,1)"
    }
  },
  "theme": "edgeless",
  "content": {
    "message": "This website is using cookies",
    "dismiss": "I AGREE",
    "link": "READ MORE",
    "href": "https://bcommassociation.com/laravel-prod/storage/app/bComm_Terms_and_Conditions.pdf"
  }
})});
</script>
