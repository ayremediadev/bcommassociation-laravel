<?php 
$baseURL = 'https://bcommassociation.com'; 
 $csspath = 'https://bcommassociation.com';
 $base = URL::to('/');
  $path = str_replace("/public","",$base); 

function imgs($file) {
  $base = 'https://bcommassociation.com';
  //$path = str_replace("/public","",$base); 
  $img = $base . Storage::url('app/img/' . $file);
  echo $img; 
} 

Route::get('user/profile', ['as' => 'my-profile', 'uses' => 'UserController@profile']);

$id = Auth::user()->id;
$name = Auth::user()->name;
$email = Auth::user()->email;
$position = Auth::user()->position;
$company = Auth::user()->company;
$location = Auth::user()->location;
$avatar = Auth::user()->avatar;
$tagline = Auth::user()->tagline; 
$presentation = Auth::user()->presentation; 
$bchprofile = Auth::user()->bch_profile; 
$interests = Auth::user()->interests; 
$opportunities = Auth::user()->opportunities; 
$experiences = Auth::user()->experiences; 
$connections = Auth::user()->connections; 
$phone = Auth::user()->phone; 
$messages = Auth::user()->messages;

?>

  <main role="main" id="profile">

  <section class="profile">
    <div class="container py-5" style="padding-top: 2em !important;">
      <div class="row">

        <div class="col-md-3">

          <div class="card user-card text-center mb-4 shadow-md margin-top-50">
            <div class="user-pic pic-lg mb-1">
              <img src="<?php avatar($avatar); ?>">
            </div>
            <div class="card-body">
              <div class="ls-1 text-uppercase font-weight-bold mb-2">{{ Auth::user()->name }}</div>
              <div class="mb-2"><?php if($position){ echo $position; ?> at <?php echo $company; } ?></div>
              <div class="font-weight-light mb-4"><?php echo $location; ?></div>
              <div class="dropdown-divider mb-4"></div>
              <div class="tagline font-weight-light mb-4"><?php echo $tagline; ?></div>
              <button type="button" class="btn btn-outline-gradient-lg-green mb-2" onclick="location.href='<?php echo $base; ?>/edit-profile'"><span>Edit profile</span></button>
              <button type="button" class="btn btn-transparent-lg" onclick="location.href='https://bcommassociation.com/user?id=<?php echo $id; ?>'"><span>See public profile</span></button>
            </div>
          </div>

        </div>

         <div class="col-md-9">

          <!--<div class="card-deck">
            <div class="card shadow-md" style="flex-grow:2">


              <div class="card-body d-flex">
                <div class="w-50">
                <img class="card-img-left" src="<?php //echo $csspath; ?>template/resources/img/photo.jpg" alt="" >
                </div>
                <div class="w-50 pl-4">
                  <h5 class="card-title font-weight-bold">News, events or announcements</h5>
                  <p class="card-text font-weight-light">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <p class="font-weight-light"><a href="">Read more</a></p>
                </div>
              </div>
            </div>

            <div class="card card-titled shadow-md">
              <div class="card-header bg-charcoal ">

                <div class="c100 p80 small left">
                  <span>80%</span>
                  <div class="slice">
                    <div class="bar"></div>
                    <div class="fill"></div>
                  </div>
                </div>
                <div class="card-title d-flex justify-content-between">
                  <span>Profile completion</span>
                  <a href="#"><i class="fas fa-times"></i></a></div>
                </div>
                <div class="card-body">
                  <ul class="profile-checklist list-unstyled pl-0 m-0">
                    <li class="d-flex mb-3"><i class="fas fa-check-circle mr-2"></i><span>Update your profile information</span></li>
                    <li class="d-flex mb-3"><i class="fas fa-check-circle mr-2"></i><span>Add your interests</span></li>
                    <li class="d-flex mb-3"><i class="far fa-circle mr-2"></i><span>Add your past experiences</span></li>
                    <li class="d-flex mb-0"><i class="far fa-circle mr-2"></i><span>Invite collaborators to join</span></li>
                  </ul>
                </div>
              </div>
          </div>-->

          <div class="text-center my-5" style="margin-top: 0 !important;">
            <div class="font-weight-bold">Recent Members</div>
          </div>
           

          <div class="card-deck">
          <?php 
          $users = DB::table('users')
                ->orderBy('id', 'desc')
                ->take(3)
                ->where('id', '!=', $id)
                ->get(); 
          
          //print_r($bcomm_connections);          
          foreach($users as $user){
            $bcomm_connections = DB::table('bcomm_connections')
                    ->where('user_id_1', '=', $id, 'OR', 'user_id_2', '=', $id)
                    ->get();
           ?>
            <div class="card user-card text-center my-5 shadow-md">
              <div class="user-pic pic-lg mb-1">
                 <img src="<?php avatar($user->avatar); ?>">
              </div>
              <div class="card-body">
                <div class="ls-1 text-uppercase font-weight-bold mb-2"> <a style="color: #000;" href="https://bcommassociation.com/user?id=<?php echo $user->id; ?>"><?php echo $user->name; ?></a> </div>
                <div class="mb-2"><?php if($user->position){ echo $user->position; ?> at <?php echo $user->company; } else { echo '<br/>'; } ?><br/>
										<?php if($user->location) { echo $user->location; } ?></div>
                <div class="dropdown-divider mb-4"></div>
                <!---<div class="tagline font-weight-light mb-4"><?php //echo $user->tagline; ?></div>--->
                <?php 
                  if(Auth::user()->connections == 0){ ?>
                    <form method="get" id="connect-mbs" target="sentinvite" action="<?php echo $base; ?>/send-connection">
						<input type="hidden" name="connection" value="<?php echo $connections + 1; ?>">
						<input type="hidden" name="user1" value="<?php echo $id; ?>">
						<input type="hidden" name="user2" value="<?php echo $user->id;?>">
						<input type="hidden" name="user1em" value="<?php echo $email; ?>">
                        <input type="hidden" name="user2em" value="<?php echo $user->email;?>">
						<input type="hidden" name="user1nm" value="<?php echo $name; ?>">
                        <input type="hidden" name="user2nm" value="<?php echo $user->name;?>">
						<input type="hidden" name="user2msgs" value="<?php echo $user->messages;?>">
						<input type="hidden" name="user2unread" value="<?php echo $user->unread_messages;?>">
						<?php if($id == 384){ ?><button type="submit" class="btn btn-outline-gradient-lg-green" onClick="window.location.reload()" value="submit"><span><img src="<?php echo $path; ?>/laravel-prod/template/resources/img/bcomm-icon.svg" class="btn-icon"> Connect</span></button><?php } ?>
                    </form>
            <?php }else{

                  $array = array();
                  foreach ($bcomm_connections as $bcomm_connection) {
                   if( $bcomm_connection->user_id_1 != $id){
                      
                      array_push($array, $bcomm_connection->user_id_1);
                    }
                    if($bcomm_connection->user_id_2 != $id ){
                      array_push($array, $bcomm_connection->user_id_2);
                    }
                  }
                 // print_r($array);
                  
                    if(!in_array($user->id, $array)){ ?>
                      <form method="get" id="connect-mbs" target="sentinvite" action="<?php echo $base; ?>/send-connection">
                        <input type="hidden" name="connection" value="<?php echo $connections + 1; ?>">
                        <input type="hidden" name="user1" value="<?php echo Auth::user()->id; ?>">
                        <input type="hidden" name="user2" value="<?php echo $user->id;?>">
						<input type="hidden" name="user1em" value="<?php echo Auth::user()->email; ?>">
                        <input type="hidden" name="user2em" value="<?php echo $user->email;?>">
						<input type="hidden" name="user1nm" value="<?php echo Auth::user()->name; ?>">
                        <input type="hidden" name="user2nm" value="<?php echo $user->name;?>">
						<input type="hidden" name="user2msgs" value="<?php echo $user->messages;?>">
						<input type="hidden" name="user2unread" value="<?php echo $user->unread_messages;?>">
						<?php if($id == 384){ ?><button type="submit" class="btn btn-outline-gradient-lg-green" onClick="window.location.reload()" value="submit" ><span><img src="<?php echo $path; ?>/laravel-prod/template/resources/img/bcomm-icon.svg" class="btn-icon" onClick="window.location.reload()"> Connect</span></button><?php } ?>
                    </form> 
                  <?php }else{
                    foreach ($bcomm_connections as $bcomm_connection) {
                        switch ($bcomm_connection->connection) {
                        case '1':
                          if($bcomm_connection->user_id_1 == $id && $bcomm_connection->user_id_2 == $user->id){ ?>
                            <?php if($id == 384){ ?><button type="button" class="btn btn-outline-gradient-lg-green" disabled><span><img src="<?php echo $path; ?>/template/resources/img/bcomm-icon.svg" class="btn-icon"> Invite sent</span></button><?php } ?>
                    <?php }elseif ($bcomm_connection->user_id_2 == $id && $user->id == $bcomm_connection->user_id_1) { ?>
							<?php if($id == 384){ ?><button type="button" class="btn btn-outline-gradient-lg-green" disabled><span><img src="<?php echo $path; ?>/template/resources/img/bcomm-icon.svg" class="btn-icon"> Accept request</span></button><?php } ?>
                    <?php }break;
                        case '2':
                          if($bcomm_connection->user_id_1 == $id && $bcomm_connection->user_id_2 == $user->id){ ?>
							<?php if($id == 384){ ?><button type="button" class="btn btn-outline-gradient-lg-green" disabled><span><img src="<?php echo $path; ?>/template/resources/img/bcomm-icon.svg" class="btn-icon"> Connected</span></button><?php } ?>
                    <?php }break;

                      }
                  }

                  }
                
              }
              ?>
                  </div>
				
				</div>
				
                  <?php } ?>
            </div>
			
			<div style="text-align: center; text-transform: uppercase;" class="col-md-12">
				
				<h6><a style="color: #000; font-weight: bold;" href="https://bcommassociation.com/members">See all members</a></h6>
				
			</div>
			
          </div>

        </div>

      </section>
	  
	  <?php if($id == 384){ ?><iframe src="<?php echo $base; ?>/send-connection" name="sentinvite" style="display: none;"></iframe><?php } ?>
	  
  </main>