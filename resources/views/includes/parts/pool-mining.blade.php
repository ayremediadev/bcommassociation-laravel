<div id="pool-mining" class="container">
    <div class="col col-lg-6 col-md-12 col-sm-12 col-xs-12">
    	<img src="<?php img('bitcoinsv-svpool.png'); ?>">
    </div>
    <div class="col col-lg-6 col-md-12 col-sm-12 col-xs-12 txt">
    	<hr align="right" class="desktop">
        <hr align="left" class="mobile">
    	<h1 class="txtupper">pool mining</h1>
    	<p>The bComm Association is committed to making Bitcoin become the new world money, a peer-to-peer electronic cash that is used globally for bCommerce.  We believe in the Satoshi Vision and safeguarding Bitcoin from unnecessary diversion from its original protocol and core design released by Satoshi Nakamoto. </p>
    	<p>As such, we proudly support Bitcoin SV (BSV), the chain and token that represents the original Bitcoin.  Reflecting its mission to fulfil the “Satoshi Vision,” BSV was created to restore the original Satoshi protocol, keep it stable and enable it to massively scale.  BSV restores the original vision to ignite the future of Bitcoin and bCommerce.</p>
    	<p>Bitcoin SV is made available for usage under the open source MIT License.</p>

    	<span>Want to mine at higher yields?</span>
        <a class="bttn" href="https://svpool.com/" target="_blank">get bitcoin sv now!</a>
    </div>
</div>




