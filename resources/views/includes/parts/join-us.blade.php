<div id="join-us" class="container">
	<div class="row">
	    <div class="col col-lg-6 col-md-12 col-sm-12 col-xs-12 txt-left">
	    	<div class="content">
		    	<span>Ready to join ?</span>
		    	<p>Creating your profile take only a few moments.</p>
		    	<p>Become part of a network of hundreds of bCommerce experts around the world, helping grow our collaborative industry.</p>
	    	</div>
	    </div>
	    <div class="col col-lg-6 col-md-12 col-sm-12 col-xs-12 txt-right">
	    	<div class="content">
	    		<a class="bttn" href="<?php url_replace(); ?>register">join us</a>
	    	</div>
	    </div>
	</div>
</div>