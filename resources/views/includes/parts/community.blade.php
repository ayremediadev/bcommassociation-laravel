<div id="community">
	<div class="col col-lg-8 col-md-12 col-sm-12 col-xs-12">
		<hr align="left">
		<h1 class="txtupper">Why join the community?</h1>
		<p>Our platform aims to bring together all crucial actors of bCommerce, and present them with unique opportunities to develop their presence within the industry</p>
	</div>

	<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12 groups">
		<div class="col col-lg-3 col-md-6 col-sm-6 col-xs-12 group">
			<img src="<?php icon('network.png'); ?>">
			<p>Grow your <span class="difcolor">Network</span></p>
			<p>Business is always about who you know</p>
		</div>
		<div class="col col-lg-3 col-md-6 col-sm-6 col-xs-12 group">
			<img src="<?php icon('briefcase.png'); ?>">
			<p>Find <span class="difcolor">Jobs</span> or Hire <span class="difcolor">Talent</span></p>
			<p>Unique skill sets require a unique platform.</p>
		</div>
		<div class="col col-lg-3 col-md-6 col-sm-6 col-xs-12 group">
			<img src="<?php icon('connecting.png'); ?>">
			<p>Connecting <span class="difcolor">Markets</span></p>
			<p>Making it easy for buyers to find suppliers</p>
		</div>
		<div class="col col-lg-3 col-md-6 col-sm-6 col-xs-12 group">
			<img src="<?php icon('collaborate.png'); ?>">
			<p><span class="difcolor">Collaborate</span></p>
			<p>Find help and projects you didn't even know existed</p>
		</div>
	</div>

</div>