 <?php  
$baseURL = 'https://bcommassociation.com'; 
$csspath = 'https://bcommassociation.com';

function imgs($file){
    $base = URL::to('/');
    $path = str_replace("/public","",$base); 
    $img = $base . '/laravel-prod' . Storage::url('app/img/' . $file);
    echo $img; 
} 

//Route::get('user/profile', ['as' => 'my-profile', 'uses' => 'UserController@profile']); 
//Route::post('/edit-profile', 'UserController@update_avatar'); 

$id = Auth::user()->id; 
$title = Auth::user()->title; 
$name = Auth::user()->name;
$email = Auth::user()->email;
$namLen = strlen($name);
$spacePos = strpos($name, " ");
$fname = substr($name, 0, $spacePos);
$lname = substr($name, $spacePos, $namLen);
$position = Auth::user()->position;
$company = Auth::user()->company;
$location = Auth::user()->location;
$avatar = Auth::user()->avatar;
$tagline = Auth::user()->tagline; 
$presentation = Auth::user()->presentation; 
$bchprofile = Auth::user()->bch_profile; 
$interests = Auth::user()->interests; 
$opportunities = Auth::user()->opportunities; 
$experiences = Auth::user()->experiences; 
$connections = Auth::user()->connections; 
$phone = Auth::user()->phone; 

?>

  <main role="main" id="profile">

  <section class="profile">

    <div class="container py-5">
      <div class="row">
        <a href="https://bcommassociation.com/home" class="text-link" style="color: #000; top:0"><i class="fas fa-long-arrow-alt-left"></i> Back</a>
        <div class="col-md-3">

          <div class="card user-card text-center mb-4 shadow-md margin-top-50">
            <div class="user-pic pic-lg mb-1">
              <img src="<?php avatar($avatar); ?>">
            </div>
			<?php if($avatar !== 'default01.jpg'){ ?>
			<form action="https://bcommassociation.com/defaultAvatar" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <input type="hidden" class="form-control-file" name="default" id="defaultAvatarFile" value="default" aria-describedby="fileHelp">
                </div>
                <button type="submit" class="btn btn-outline-gradient-lg-green mb-2"><span>Remove avatar</span></button>
            </form>
            <?php } ?>
            <div class="card-body">
              <div class="ls-1 text-uppercase font-weight-bold mb-2">{{ Auth::user()->name }}</div>
              <div class="mb-2"><?php echo $position; ?> at <?php echo $company; ?></div>
              <div class="font-weight-light mb-4"><?php echo $location; ?></div>
			  <form action="https://bcommassociation.com/profile" method="post" enctype="multipart/form-data" style="padding: 1em;">
				<p><strong>Upload new profile image</strong></p>
                @csrf
                <div class="form-group">
                    <input type="file" class="form-control-file" name="avatar" id="avatarFile" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
                </div>
                <button type="submit" class="btn btn-outline-gradient-lg-green mb-2"><span>Change avatar</span></button>
            </form>
              <div class="dropdown-divider mb-4"></div>
              <div class="font-weight-light mb-4"><?php echo $tagline; ?></div>
              <button type="button" class="btn btn-outline-gradient-lg-green mb-2" onclick="location.href='https://bcommassociation.com/user?id=<?php echo $id; ?>'"><span>See public profile</span></button>
            </div>
          </div>

        </div>

        <div class="col-md-6">

          <div class="card mb-4 shadow-md">
            <div class="card-body">
              <div class="d-flex justify-content-between align-items-center mb-4">
                <div class="font-weight-bold">Personal Information</div>
                <button type="button" class="btn btn-solid-sm-charcoal" id="editButton"><i class="fas fa-pencil-alt"></i>Edit</button>
              </div>

              <div id="editProfile">

                  <form class="container" id="updateInfo" onsubmit="submittedUpdate()" style=" font-size: .8em;" method="get" target="update_frame" action="https://bcommassociation.com/update-profile">

                    <input type="hidden" value="<?php echo $id; ?>" name="id">

                    <div class="row align-items-center justify-content-between mb-3">

                      <div class="col-auto">
                        <div class="form-group">
                          <label for="personalinfo_Title">Title</label>
                          <select class="form-control form-control-sm" id="personalinfo_Title" name="title" disabled>
                            <?php $titles = array('Mr', 'Ms', 'Mrs', 'Miss', 'Dr', 'Prof'); 
                            foreach($titles as $t){ ?>
                              <option value="<?php echo $t . ' '; ?>"
                                  <?php if($t == $title){ ?>
                                      selected="selected"
                                  <?php } ?>
                              ><?php echo ' ' . $t; ?>.</option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="col">
                        <div class="form-group">
                          <label for="personalinfo_Title">First Name</label>
                          <input type="text" class="form-control form-control-sm" id="personalinfo_FirstName" name="fname" value="<?php echo $fname; ?>" readonly>
                        </div>
                      </div>

                      <div class="col">
                        <div class="form-group">
                          <label for="personalinfo_Title">Last Name</label>
                          <input type="text" class="form-control form-control-sm" id="personalinfo_LastName" name="lname" value="<?php echo $lname; ?>" readonly>
                        </div>
                      </div>

                    </div>

                    <div class="row align-items-center justify-content-between mb-3">
                      <div class="col">
                        <div class="form-group">
                          <label for="personalinfo_Email">Email</label>
                          <input type="email" class="form-control form-control-sm" id="personalinfo_Email" name="email" aria-describedby="emailHelp" value="<?php echo $email; ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="row align-items-center justify-content-between mb-3">
                      <div class="col">
                        <div class="form-group">
                          <label for="personalinfo_Title">Phone number</label>
                          <input type="text" name="phone" class="form-control form-control-sm" id="personalinfo_Phone" value="<?php echo $phone; ?>" readonly>
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-group">
                          <label for="personalinfo_Location">Location</label>
                          <input type="text" name="location" class="form-control form-control-sm" id="personalinfo_Location" value="<?php echo $location; ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="row align-items-center justify-content-between mb-3">
                      <div class="col">
                        <div class="form-group">
                          <label for="personalinfo_Position">Position</label>
                          <input type="text" name="position" class="form-control form-control-sm" id="personalinfo_Position" value="<?php echo $position; ?>" readonly>
                        </div>
                      </div>

                      <div class="col">
                        <div class="form-group">
                          <label for="personalinfo_Company">Company</label>
                          <input type="text" name="company" class="form-control form-control-sm" id="personalinfo_Company" value="<?php echo $company; ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="row align-items-center justify-content-between mb-3">
                      <div class="col">
                        <div class="form-group">
                          <label for="personalinfo_Bio">Bio</label>
                          <textarea type="text" name="tagline" class="form-control form-control-sm" id="personalinfo_Bio" rows="5" name="tagline" maxlength="100" placeholder="Maximum 100 characters" readonly><?php echo $tagline; ?></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row align-items-center justify-content-between mx-0 btn-wrapper">
                      <button type="submit" class="col-lg-6 btn btn-solid-lg-charcoal" onclick="clicked()">Save</button>
                    </div>
                  </form>

                  <iframe name="update_frame" src="<?php echo URL::to('/'); ?>/update-profile" style="display: none;" >
                  </iframe>

              </div>

            </div>
          </div>

          <div class="card mb-4 shadow-md">

            <div class="card-body">

              <div class="d-flex justify-content-between align-items-center mb-4">
                <div class="font-weight-bold">Interests</div>
            <!--<button type="button" class="btn btn-solid-sm-charcoal">Done</button> -->
                <button type="button" class="btn btn-solid-sm-charcoal" id="editInt"><i class="fas fa-pencil-alt"></i>Edit</button>
              </div>

              <div>
                <?php $intArray = array('mining', 'development', 'cryptocurrencies', 'blockchain', 'tech', 'fintech', 'bitcoin', 'bitcoin cash'); 
                foreach($intArray as $int){
                    if(strpos($interests, $int) !== false){ ?>
                        <form class="container updateInt" style="display: inline-block; width: auto; font-size: .8em; margin-bottom: 1em;" method="get" target="updateint_frame" action="https://bcommassociation.com/remove-interest">
                            <input type="hidden" name="remove" value="<?php echo $int; ?>" />
                            <input type="hidden" name="current" value="current: <?php echo $interests; ?>" />
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <button type="submit" class="btn btn-outline-gradient-sm-green tags interests" disabled><span><?php echo $int; ?> <i class="fas fa-times"></i></a></button>
                        </form>
                    <?php } else { ?>
                        <form class="container updateInt" style="display: inline-block; width: auto; font-size: .8em; margin-bottom: 1em;" method="get" target="updateint_frame" action="https://bcommassociation.com/update-interests">
                            <input type="hidden" name="add" value="<?php echo $int; ?>" />
                            <input type="hidden" name="current" value="<?php echo $interests; ?>" />
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <button type="submit" class="btn btn-outline-sm tags interests" disabled><span><?php echo $int; ?></span></button>
                        </form>
                    <?php } 
                } ?>
                <iframe name="updateint_frame" src="https://bcommassociation.com/update-interests" style="display: none;" >
                </iframe>

              </div>

              </div>
            </div>

            <!---<div class="card mb-4 shadow-md">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-center mb-4">
                  <div class="font-weight-bold">Experiences</div>
                  </div>

                <div class="row align-items-center justify-content-between">
                  <div class="col-auto my-2">
                    <div class="company-logo shadow-md">
                      <img src="<?php echo $csspath; ?>/resources-templates/img/6SpDPegx_400x400.jpg" class="img-fluid">
                    </div>
                  </div>
                  <div class="col">
                    <div class="experience_position"><strong>Founder &amp; Head of Design</strong></div>
                    <div class="experience_company">Mad Studio</div>
                    <div class="experience_location_tenure">Paris, France | 8 years 3 months</div>
                  </div>
                  <div class="col-auto">
                    <div>
                      <button type="submit" class="btn btn-outline-gradient-md-red"><span>Remove</span></button>
                    </div>
                  </div>
                  <div class="col-12 my-2">
                    <p class="mb-0 font-weight-light">Proin mattis facilisis velit. Fusce lacinia lorem sed magna dignissim, vel egestas neque finibus. Suspendisse posuere tincidunt libero. Curabitur nec nunc ligula. Sed nec est velit. Aliquam sed mauris ex.</p>
                  </div>
                </div>

                <div class="dropdown-divider mt-4 mb-4"></div>

                <div class="row align-items-center justify-content-between">
                  <div class="col-auto my-2">
                    <div class="company-logo shadow-md">
                      <img src="<?php echo $csspath; ?>/resources-templates/img/cg.png" class="img-fluid">
                    </div>
                  </div>
                  <div class="col">
                    <div class="experience_position"><strong>Designer</strong></div>
                    <div class="experience_company">CoinGeek Group</div>
                    <div class="experience_location_tenure">London, UK | 6 months</div>
                  </div>
                  <div class="col-auto">
                    <div>
                      <button type="submit" class="btn btn-outline-gradient-md-red"><span>Remove</span></button>
                    </div>
                  </div>
                  <div class="col-12 my-2">
                    <p class="mb-0 font-weight-light">Proin mattis facilisis velit. Fusce lacinia lorem sed magna dignissim, vel egestas neque finibus. Suspendisse posuere tincidunt libero. Curabitur nec nunc ligula. Sed nec est velit. Aliquam sed mauris ex.</p>
                  </div>
                </div>

                <div class="mt-4">
                  <button type="button" class="btn btn-outline-gradient-lg-green" id="editXP"><span>Add an experience</span></button>
                </div>

                <form>

                    <div class="row align-items-center justify-content-between mb-3">
                      <div class="col">
                        <div class="form-group">
                          <label for="experience_Location">Company Picture</label>
                          <div class="d-flex">
                            <div><i class="fas fa-plus form-upload"></i></div>
                            <div class="font-weight-light mt-3">JPG and PNG (280x280 recommended)<br>max size 100kb</div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row align-items-center justify-content-between mb-3">
                      <div class="col">
                        <div class="form-group">
                          <label for="experience_Position">Position</label>
                          <input type="text" class="form-control form-control-sm" id="experience_Position" placeholder="Position">
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-group">
                          <label for="experience_Company">Company</label>
                          <input type="text" class="form-control form-control-sm" id="experience_Company" placeholder="Company">
                        </div>
                      </div>
                    </div>

                    <div class="row align-items-center justify-content-between mb-3">
                      <div class="col">
                        <div class="form-group">
                          <label for="experience_Location">Location</label>
                          <input type="email" class="form-control form-control-sm" id="experience_Location" placeholder="Location">
                        </div>
                      </div>
                    </div>

                    <div class="row align-items-center justify-content-between mb-3">
                      <div class="col">
                        <div class="form-group">
                          <label for="experience_From">From</label>
                          <input type="text" class="form-control form-control-sm" id="experience_From" placeholder="month/year">
                        </div>
                      </div>

                      <div class="col">
                        <div class="form-group">
                          <label for="experience_To">To</label>
                          <input type="text" class="form-control form-control-sm" id="experience_To" placeholder="month/year">
                        </div>
                      </div>
                    </div>

                    <div class="row align-items-center justify-content-between mb-3">
                      <div class="col">
                        <div class="form-group">
                          <label for="experience_Bio">Bio</label>
                          <textarea type="text" class="form-control form-control-sm" id="experience_Bio" rows="5" placeholder="Maximum XX characters"></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row align-items-center justify-content-between mx-0 btn-wrapper">
                      <button type="submit" class="col-lg-6 btn btn-solid-lg-charcoal py-3">Add experience</button>
                    </div>
                  </form>
                  --->

              </div>
            </div>

          </div>

          <div class="col-md-3">

            <!--<div class="card card-titled mb-4 shadow-md">
              <div class="card-header bg-charcoal ">

                <div class="c100 p80 small left">
                  <span>80%</span>
                  <div class="slice">
                    <div class="bar"></div>
                    <div class="fill"></div>
                  </div>
                </div>

                <div class="card-title d-flex justify-content-between">
                  <span>Profile completion</span>
                  <a href="#"><i class="fas fa-times"></i></a></div>

                </div>

                <div class="card-body">
                  <ul class="profile-checklist list-unstyled pl-0 m-0">
                    <li class="d-flex mb-3"><i class="fas fa-check-circle mr-2"></i><span>Update your profile information</span></li>
                    <li class="d-flex mb-3"><i class="fas fa-check-circle mr-2"></i><span>Add your interests</span></li>
                    <li class="d-flex mb-3"><i class="far fa-circle mr-2"></i><span>Add your past experiences</span></li>
                    <li class="d-flex mb-0"><i class="far fa-circle mr-2"></i><span>Invite collaborators to join</span></li>
                  </ul>

                </div>
              </div>
            </div>-->

          </div>

        </div>

      </section>

    
  </main>