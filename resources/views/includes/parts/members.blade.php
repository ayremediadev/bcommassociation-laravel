 <?php 
$baseURL = 'https://bcommassociation.com'; 
$csspath = 'https://bcommassociation.com';
 // $base = URL::to('/');
 //  $path = str_replace("/public","",$base); 

function imgs($file) {
  // $base = URL::to('/');
  // $path = str_replace("/public","",$base); 
  $baseURL = 'https://bcommassociation.com'; 
  $img = $baseURL . "/laravel-prod" . Storage::url('app/img/' . $file);
  echo $img; 
} 
Route::get('user/profile', ['as' => 'my-profile', 'uses' => 'UserController@profile']);

$id = Auth::user()->id; 
$name = Auth::user()->name;
$position = Auth::user()->position;
$company = Auth::user()->company;
$location = Auth::user()->location;
$avatar = Auth::user()->avatar;
$tagline = Auth::user()->tagline; 
$presentation = Auth::user()->presentation; 
$bchprofile = Auth::user()->bch_profile; 
$interests = Auth::user()->interests; 
$opportunities = Auth::user()->opportunities; 
$experiences = Auth::user()->experiences; 
$connections = Auth::user()->connections; 
$phone = Auth::user()->phone; 

?>

<main role="main" style="margin-top: 20px">
  <section class="search_member bg-charcoal">
    <div class="container">
        <div class="row py-5">
            <div class="col-md-3 position-relative">
                <form method="get" id="filter-profile" target="profile_result" action="<?php echo $baseURL; ?>/bch-filtered">
                    <div class="card user-card mb-4 shadow-md filter-block position-absolute">
                        <div class="card-body" style="height: 100vh;">
                            <div class="row align-items-center justify-content-start box-header mb-3 text-left">
                                <div class="col"><h3 class="my-0">bComms Profile</h3></div>
                            </div>
                            <div class="row align-items-center justify-content-start text-left text-uppercase mb-4">
                                <div class="col">
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_AllProfile" name="profile[]" value="allprofiles">
                                        <label class="form-check-label" for="profile_AllProfile">All Profiles</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_Merchant" name="profile[]" value="Merchant">
                                        <label class="form-check-label" for="profile_Merchant">Merchant</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_Miner" name="profile[]" value="Miner">
                                        <label class="form-check-label" for="profile_Miner">Miner</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_Exchange" name="profile[]" value="Exchange">
                                        <label class="form-check-label" for="profile_Exchange">Hedge Fund / Exchange</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input profile_media" id="profile_Media" name="profile[]" value="Media">
                                        <label class="form-check-label" for="profile_Media">Media</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input profile_developer" id="profile_Developer" name="profile[]" value="Developer">
                                        <label class="form-check-label" for="profile_Developer">Developer</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_VentureCapitalist" value="Venture Capitalist" name="profile[]">
                                        <label class="form-check-label" for="profile_VentureCapitalist">Venture Capitalist</label>
                                    </div>
                                    <!--<div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_OtherProfile">
                                        <label class="form-check-label" for="profile_OtherProfile">Other Profile</label>
                                    </div>-->
                                    <!---<div id="More_Profile" class="collapse">
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="profile_Exchange">
                                            <label class="form-check-label" for="profile_Exchange">Hedge Fund / Exchange</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="profile_Media">
                                            <label class="form-check-label" for="profile_Media">Media</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="profile_Developer">
                                            <label class="form-check-label" for="profile_Developer">Developer</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="profile_VentureCapitalist">
                                            <label class="form-check-label" for="profile_VentureCapitalist">Venture Capitalist</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="profile_OtherProfile">
                                            <label class="form-check-label" for="profile_OtherProfile">Other Profile</label>
                                        </div>
                                    </div>
                                    <button class="showmore_collapse" type="button" data-toggle="collapse" data-target="#More_Profile" aria-expanded="false" aria-controls="collapseExample">
                                        Show More <i class="fas fa-angle-down"></i>
                                    </button>--->
                                </div>
                            </div>

                           <!--  <div class="row align-items-center justify-content-start box-header mb-3 text-left">
                                <div class="col"><h3 class="my-0">Region</h3></div>
                            </div>
                            <div class="row align-items-center justify-content-start text-left text-uppercase mb-4">
                                <div class="col">
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="region_AllRegions">
                                        <label class="form-check-label" for="region_AllRegions">All Regions</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="region_Emea" name="region[]" value="Emea">
                                        <label class="form-check-label" for="region_Emea">Emea</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="region_Apac" name="region[]" value="Apac">
                                        <label class="form-check-label" for="region_Apac">Apac</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="region_Nam" name="region[]" value="Nam">
                                        <label class="form-check-label" for="region_Nam">Nam</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="region_Latam" name="region[]" value="Latam">
                                        <label class="form-check-label" for="region_Latam">Latam</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="region_Paris" name="region[]" value="Paris">
                                        <label class="form-check-label" for="region_Paris">Paris</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="region_Shanghai" name="region[]" value="Shanghai">
                                        <label class="form-check-label" for="region_Shanghai">Shanghai</label>
                                    </div>
                                    <div id="More_Region" class="collapse">
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Emea" name="region[]" value="Emea">
                                            <label class="form-check-label" for="region_Emea">Emea</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Apac" name="region[]" value="Apac">
                                            <label class="form-check-label" for="region_Apac">Apac</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Nam" name="region[]" value="Nam">
                                            <label class="form-check-label" for="region_Nam">Nam</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Latam" name="region[]" value="Latam">
                                            <label class="form-check-label" for="region_Latam">Latam</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Paris" name="region[]" value="Paris">
                                            <label class="form-check-label" for="region_Paris">Paris</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Shanghai" name="region[]" value="Shanghai">
                                            <label class="form-check-label" for="region_Shanghai">Shanghai</label>
                                        </div>
                                    </div>
                                    <button class="showmore_collapse" type="button" data-toggle="collapse" data-target="#More_Region" aria-expanded="false" aria-controls="collapseExample">
                                        Show More <i class="fas fa-angle-down"></i>
                                    </button>
                                </div>
                            </div> -->

                            <!--<div class="row align-items-center justify-content-start box-header mb-3 text-left">
                                <div class="col"><h3 class="my-0">City</h3></div>
                            </div>
                            <div class="row align-items-center justify-content-start box-header mb-3 text-left">
                                <div class="col">
                                    <div class="form-group search-block">
                                        <input type="text" class="search-input" id="" placeholder="Search for a city">
                                        <button class="search-button"><i class="fas fa-search"></i></button>
                                    </div>

                                </div>
                            </div>
                            <div class="row align-items-center justify-content-start text-left text-uppercase mb-4">
                                <div class="col">
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_AllProfile" name="city[]" value="allcities">
                                        <label class="form-check-label" for="profile_AllProfile">All Cities</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_Merchant" name="city[]" value="London">
                                        <label class="form-check-label" for="profile_Merchant">London</label>
                                    </div>
									<div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_Merchant" name="city[]" value="Manila">
                                        <label class="form-check-label" for="profile_Merchant">Manila</label>
                                    </div>
									<div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_Merchant" name="city[]" value="Berlin">
                                        <label class="form-check-label" for="profile_Merchant">Berlin</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_Merchant" name="city[]" value="Houston">
                                        <label class="form-check-label" for="profile_Merchant">Houston</label>
                                    </div>
									<div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_Miner" name="city[]" value="Singapore">
                                        <label class="form-check-label" for="profile_Miner">Singapore</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_Exchange" name="city[]" value="New York">
                                        <label class="form-check-label" for="profile_Exchange">New York City</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_Media" name="city[]" value="Hongkong">
                                        <label class="form-check-label" for="profile_Media">Hongkong</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_Developer" name="city[]" value="Paris">
                                        <label class="form-check-label" for="profile_Developer">Paris</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="profile_VentureCapitalist" name="city[]" value="Shanghai">
                                        <label class="form-check-label" for="profile_VentureCapitalist">Shanghai</label>
                                    </div>
                                    <div id="More_Region" class="collapse">
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Emea">
                                            <label class="form-check-label" for="region_Emea">Emea</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Apac">
                                            <label class="form-check-label" for="region_Apac">Apac</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Nam">
                                            <label class="form-check-label" for="region_Nam">Nam</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Latam">
                                            <label class="form-check-label" for="region_Latam">Latam</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Paris">
                                            <label class="form-check-label" for="region_Paris">Paris</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Shanghai">
                                            <label class="form-check-label" for="region_Shanghai">Shanghai</label>
                                        </div>
                                    </div>
                                    <button class="showmore_collapse" type="button" data-toggle="collapse" data-target="#More_Region" aria-expanded="false" aria-controls="collapseExample">
                                        Show More <i class="fas fa-angle-down"></i>
                                    </button>
                                </div>
                            </div>--->
							
							<div class="row align-items-center justify-content-start box-header mb-3 text-left">
                                <div class="col"><h3 class="my-0">Interest</h3></div>
                            </div>
                            <!--<div class="row align-items-center justify-content-start box-header mb-3 text-left">
                                <div class="col">
                                    <div class="form-group search-block">
                                        <input type="text" class="search-input" id="" placeholder="Search for an interest">
                                        <button class="search-button"><i class="fas fa-search"></i></button>
                                    </div>

                                </div>
                            </div>-->
                            <div class="row align-items-center justify-content-start text-left text-uppercase">
                                <div class="col">
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="interest_AllInterest" name="interests[]" value="allinterests">
                                        <label class="form-check-label" for="interest_AllInterest">All Interest</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="interest_Tech" name="interest[]" value="Tech">
                                        <label class="form-check-label" for="interest_Tech">Tech</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="interest_Fintech" name="interest[]" value="Fintech">
                                        <label class="form-check-label" for="interest_Fintech">Fintech</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="interest_Bitcoin" name="interest[]" value="Bitcoin">
                                        <label class="form-check-label" for="interest_Bitcoin">Bitcoin</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="interest_BitcoinSV" name="interest[]" value="BitcoinSV">
                                        <label class="form-check-label" for="interest_BitcoinCash">Bitcoin SV</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="interest_Blockchain" name="interest[]" value="Blockchain">
                                        <label class="form-check-label" for="interest_Blockchain">Blockchain</label>
                                    </div>
                                    <div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="interest_Mining" name="interest[]" value="Mining">
                                        <label class="form-check-label" for="interest_Mining">Mining</label>
                                    </div>
                                    <!--<div class="form-group form-check my-0">
                                        <input type="checkbox" class="form-check-input" id="interest_OtherProfile" name="interest[]" value="Other Profile">
                                        <label class="form-check-label" for="interest_OtherProfile">Other Profile</label>
                                    </div>
                                    <div id="More_Interest" class="collapse">
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Emea">
                                            <label class="form-check-label" for="region_Emea">Emea</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Apac">
                                            <label class="form-check-label" for="region_Apac">Apac</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Nam">
                                            <label class="form-check-label" for="region_Nam">Nam</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Latam">
                                            <label class="form-check-label" for="region_Latam">Latam</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Paris">
                                            <label class="form-check-label" for="region_Paris">Paris</label>
                                        </div>
                                        <div class="form-group form-check my-0">
                                            <input type="checkbox" class="form-check-input" id="region_Shanghai">
                                            <label class="form-check-label" for="region_Shanghai">Shanghai</label>
                                        </div>
                                    </div>
                                    <button class="showmore_collapse" type="button" data-toggle="collapse" data-target="#More_Interest" aria-expanded="false" aria-controls="collapseExample">
                                        Show More <i class="fas fa-angle-down"></i>
                                    </button>-->
                                </div>
                            </div>

                        </div>
                    </div>
                
                </form>
            </div>
            <div class="col-md-9">
            </div>
            
        </div>
    </div>
  </section>
  <!--<section class="invitesmembers_list">
    <div class="container py-4">
        <div class="row">
            <div class="col-md-9 offset-md-3">-->
                <!-- START PENDING INVITES -->
                <!--<div class="row align-items-center justify-content-center box-header mb-5 text-center">
                    <div class="col"><h3 class="my-0">Pending Invites</h3></div>
                </div>
                <div class="row align-items-center justify-content-center box-header mb-3 text-center">
                    <!--<div class="col-6 col-md-4">
                        <!--<div class="card user-card text-center mb-4 shadow-md margin-top-50">
                            <div class="user-pic pic-lg mb-1">
                            <img src="<?php //echo $path; ?>/template/resources/img/user-1.jpg">
                            </div>
                            <div class="card-body">
                            <div class="ls-1 text-uppercase font-weight-bold mb-2">Mehdi Boumendjel</div>
                            <div class="mb-2">Founder &amp; Head of Design Mad/Studio</div>
                            <div class="font-weight-light mb-4">Paris, France</div>
                            <div class="dropdown-divider mb-4"></div>
                            <div class="font-weight-light mb-4">I'm passionate about design and Blockchain technologies</div>
                            </div>
                        </div>-->
                    <!--</div>
                    <div class="col-6 col-md-4">-->
                        <!--<div class="card user-card text-center mb-4 shadow-md margin-top-50">
                            <div class="user-pic pic-lg mb-1">
                            <img src="<?php //echo $path; ?>/template/resources/img/user-1.jpg">
                            </div>
                            <div class="card-body">
                            <div class="ls-1 text-uppercase font-weight-bold mb-2">Mehdi Boumendjel</div>
                            <div class="mb-2">Founder &amp; Head of Design Mad/Studio</div>
                            <div class="font-weight-light mb-4">Paris, France</div>
                            <div class="dropdown-divider mb-4"></div>
                            <div class="font-weight-light mb-4">I'm passionate about design and Blockchain technologies</div>
                            </div>
                        </div>
                    </div>-->
                </div>
                <!-- END PENDING INVITES -->
                <!-- START MEMBERS MIGHT KNOW -->
                <!--<div class="row align-items-center justify-content-center box-header mb-5 text-center">
                    <div class="col"><h3 class="my-0">Members you might know</h3></div>
                </div>-->
                <!--<div class="row align-items-center justify-content-center box-header mb-3 text-center">
                
                <?php $users = DB::table('users')
                ->orderBy('id', 'desc')
                ->take(3)
                ->where('id', '!=', $id)
                ->get(); 
              foreach($users as $user){ ?>

                    <div class="col-6 col-md-4">
                        <div class="card user-card text-center mb-4 shadow-md margin-top-50">
                            <div class="user-pic pic-lg mb-1">
                            <img src="<?php avatar($user->avatar); ?>">
                            </div>
                            <div class="card-body">
                            <div class="ls-1 text-uppercase font-weight-bold mb-2"><?php echo $user->name; ?></div>
                            <div class="mb-2"><?php if ($user->position){ echo $user->position; ?> at <?php echo $user->company; } ?></div>
                            <div class="font-weight-light mb-4"><?php echo $user->location; ?></div>
                            <div class="dropdown-divider mb-4"></div>
                            <div class="font-weight-light mb-4"><?php echo $user->tagline; ?></div>
                            <button type="button" class="btn btn-outline-gradient-lg-green" onclick="location.href='<?php echo URL::to('/'); ?>/user/?id=<?php echo $user->id; ?>'"><span>See public profile</span></button>
                            </div>
                        </div>
                    </div>

              <?php } ?>
                
                </div>-->
                <!-- END MEMBERS MIGHT KNOW -->
            <!--</div>
        </div>
    </div>
  </section>-->
  <!--<section class="collab_invites">
    <div class="container py-4">
        <div class="row">
            <div class="col-md-9 offset-md-3">
                <!-- START INVITE -->
                <!--<div class="row align-items-center justify-content-center p-4 text-center collabsend_block">
                    <div class="col">
                        <div class="row align-items-center justify-content-center text-center">
                            <div class="col">
                                <h2 class="text-uppercase section-headline-h2">Your Collaborators Are Missing Out!</h2>
                                <p>Invites your collaborators in bComm community</p>
                            </div>
                        </div>
                        
                        <div class="row align-items-center justify-content-center p-4 text-center">
                            <div class="col text-left">
                                <div class="form-group">
                                    <label for="collabinvite_Email">Email</label>
                                    <input type="email" class="form-control" id="collabinvite_Email" aria-describedby="emailHelp" placeholder="Email">
                                </div>
                            </div>
                            <div class="col text-left">
                                <div class="form-group">
                                    <label for="collabinvite_FirstName">First Name</label>
                                    <input type="text" class="form-control" id="collabinvite_FirstName" aria-describedby="emailHelp" placeholder="First Name">
                                </div>
                            </div>
                            <div class="col text-left">
                                <div class="form-group">
                                    <label for="collabinvite_LastName">Last Name</label>
                                    <input type="text" class="form-control" id="collabinvite_LastName" aria-describedby="emailHelp" placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-outline-gradient-lg-green"><span>Send Invite</span></button> 
                    </div>
                </div>-->
                <!-- END INVITE -->
            </div>
        </div>
    </div>
  </section>

  <iframe id="bchprofile" name="profile_result" src="<?php url_replace(); ?>bch-filtered" style="width: 100%; height: 100vh;border:none" class="iframe-container"></iframe>

  <section class="other_bcommmembers">
   <!--  <div class="container py-4">
        <div class="row">
            <div class="col-md-9 offset-md-3">
                <!-- START BCOMM ASSOCIATE MEMBERS -->
                <!-- <div class="row align-items-center justify-content-center box-header mb-5 text-center">
                    <div class="col"><h3 class="my-0">bComm Association members</h3></div>
                </div> -->
                <div class="row align-items-center justify-content-center box-header mb-3 text-center">
                    <?php 
                        // get total numbers of users


                        // $users2 = DB::table('users')
                        // ->orderBy('id', 'desc')
                        // ->take(12)
                        // ->where('id', '!=', $id)
                        // ->get(); 

                        // foreach($users2 as $user){ 
                        ?>

                           <!--  <div class="col-6 col-md-4">
                                <div class="card user-card text-center mb-4 shadow-md margin-top-50 filterDiv profile_<?php //echo $user->bch_profile;?>">
                                    <div class="user-pic pic-lg mb-1">
                                    <img src="<?php //avatar($user->avatar); ?>">
                                    </div>
                                    <div class="card-body">
                                    <div class="ls-1 text-uppercase font-weight-bold mb-2"><?php //echo $user->name; ?></div>
                                    <div class="mb-2"><?php //echo $user->position; ?> at <?php //echo $user->company; ?></div>
                                    <div class="font-weight-light mb-4"><?php //echo $user->location; ?></div>
                                    <div class="dropdown-divider mb-4"></div>
                                    <div class="font-weight-light mb-4"><?php //echo $user->tagline; ?></div>
                                    <button type="button" class="btn btn-outline-gradient-lg-green" onclick="location.href='<?php //echo URL::to('/'); ?>/user/?id=<?php //echo $user->id; ?>'" formtarget="_parent"><span>See public profile</span></button>
                                    </div>
                                </div>
                            </div>
 -->
                        <?php // } ?>
                        

              <!--   </div>
                
                END BCOMM ASSOCIATE MEMBERS
            </div>
        </div> -->
  <!--   </div> -->
  </section>
</main>