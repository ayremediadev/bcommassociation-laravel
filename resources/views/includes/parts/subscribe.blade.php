<div id="subscribe">

    <h1 class="txtupper" style="text-align: center;">Subscribe to the bcomm association newsletter</h1>

    <br/> &nbsp;
    <form class="container" name='Newsletter bComm' style=" font-size: .8em;" method="get" target="subs_frame" action="<?php echo URL::to('/'); ?>/subscribe-process">

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <label class="txtupper">email</label>
            <input type="email" name="email" style="width: 100%; padding: .5em;" required/>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
            <label class="txtupper">first name</label>
            <input type="text" name="firstname" class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-lg-3" style="width: 100%; padding: .5em;" required/>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
            <label class="txtupper">last name</label>
            <input type="text" name="lastname" class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-lg-3" style="width: 100%; padding: .5em;" required/> <br/>&nbsp;
        </div>

        <div class="col-xs-0 col-sm-0 col-md-4 col-lg-4 col-xl-4"></div>
            
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4" style="text-align: center;">
            <input class="txtupper" id="subsBtn" type="submit" name="submit" value="signup" style="margin: auto; width: 100%; background: #fff; border: 2px solid #13CE66; padding: .8em;"/ onClick="Confirm(this.form)" />
        </div>

        <div class="col-xs-0 col-sm-0 col-md-4 col-lg-4 col-xl-4"></div>

    </form>

    <div id="message" style="text-align: center; padding: 20px; animation: expand .5s ease-in-out;"></div>

    <iframe name="subs_frame" src="<?php echo URL::to('/'); ?>/subscribe-process" style="display: none;" >
    </iframe>

</div>

<script type="text/javascript">

function Confirm(form){
document.getElementById("message").innerHTML = "You have successfully subscribed";
}
    
</script>