<?php 
	
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
$slug = substr($actual_link, 33);

if(strpos($slug, '?fbclid=') !== false){
	
	$post_slug = substr($slug, 0, strpos($slug, '?fbclid='));
	
} else {
	
	$post_slug = $slug; 
	
}
	
$allposts = (array)json_decode(file_get_contents('https://bcommassociation.com/laravel-prod/public/wp/wp-json/wp/v2/posts/'));
foreach($allposts as $post){
	$slug = $post->slug;
	if($slug == $post_slug){
		$postid = $post->id;
	} 
}

$jsondata = (array)json_decode(file_get_contents('https://bcommassociation.com/laravel-prod/public/wp/wp-json/wp/v2/posts/'.$postid.'/'));

// Title
$titleArray = (array)$jsondata['title']; 
$title = $titleArray['rendered'];

// Content
$allcontent = $jsondata['content'];
$conArray = (array)$allcontent; 
$content = $conArray['rendered'];

// Slug
$slug = $jsondata['slug'];

?>

<span id="info"></span>
<div style="margin: 0 1.2rem;"><h1 id="remote-title" style="margin-top: 1.5em; margin-bottom: 1em"><?php echo $title; ?></h1></div>
<div id="remote-content"><?php echo $content; ?></div> 