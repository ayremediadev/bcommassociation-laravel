<?php 
function icons($file){
    $base = 'https://bcommassociation.com';
    //$path = str_replace("/public","",$base); 
    $img = $base . '/laravel-prod' . Storage::url('app/img/icons/' . $file);
    echo $img; 
} 
?>
<div id="about" class="container">
	<div class="row section-title">
		<h1 class="txtupper">Who is the bComm Association designed for?</h1>
	</div>

	<div class="row groups">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 group">
				<div class="content">
					<img src="<?php icons('code.png'); ?>">
					<p>bComm is for <span class="difcolor">Developers</span></p>
					<p>Creating the apps that are the cornerstone of bCommerce.</p>
				</div>
			</div>

			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 group">
				<div class="content">
					<img src="<?php icons('diamond.png'); ?>">
					<p>bComm is for <span class="difcolor">Miners</span></p>
					<p>Making Bitcoin SV bigger every day.</p>
				</div>
			</div>

			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 group">
				<div class="content">
					<img src="<?php icons('cart.png'); ?>">
					<p>bComm is for <span class="difcolor">Merchants</span></p>
					<p>Driving Bitcoin SV mass adoption.</p>
				</div>
			</div>

			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 group">
				<div class="content">
					<img src="<?php icons('swap.png'); ?>">
					<p>bComm is for <span class="difcolor">Exchanges</span></p>
					<p>BSV/Fiat conversion is the lifeblood of bCommerce.</p>
				</div>
			</div>
	</div>
</div>