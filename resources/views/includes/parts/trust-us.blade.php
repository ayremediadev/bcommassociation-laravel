<div id="trust-us" class="container">
	<div class="row section-title">
		<h1 class="txtupper">They trust us</h1>
	</div>
	<div class="row">
		<div class="col col-lg-2 col-md-2 col-xs-2"></div>
	    <div class="col col-lg-8 col-md-8 col-xs-8">
	   		<div class="content">
			   	<p>Our platform aims to bring together all crucial actors of bCommerce, and present them with unique opportunities to develop their presence within the industry.</p>
			</div>
		</div>
		<div class="col col-lg-2 col-md-2 col-xs-2"></div>
	</div>
	<div class="row">
		<div class="col col-lg-12 col-md-12 col-xs-12 desktop">
	   		<div class="content">
				<ul class="logos">
			   		<li><img src="<?php logo('coingeek.png'); ?>"></li>
			   		<li><img src="<?php logo('nchain.png'); ?>"></li>
			   		<li><img src="<?php logo('rawpool.png'); ?>"></li>
			   		<li><img src="<?php logo('squire.png'); ?>"></li>
			   		<li><img src="<?php logo('svpool.png'); ?>"></li>
			   	</ul>
			</div>
		</div>
		 <div class="col col-lg-12 col-md-12 col-xs-12 mobile">
	   		<div class="content">
				<div id="myCarousellogo" class="row dirs mobile carousel slide" data-ride="carousel">
			        <ol class="carousel-indicators">
			          <li data-target="#myCarousellogo" data-slide-to="0" class="active"></li>
			          <li data-target="#myCarousellogo" data-slide-to="1"></li>
			          <li data-target="#myCarousellogo" data-slide-to="2"></li>
			          <li data-target="#myCarousellogo" data-slide-to="3"></li>
			          <li data-target="#myCarousellogo" data-slide-to="4"></li>
			        </ol>
       
					<ul class="logos carousel-inner ">
				   		<li class="item active"><img src="<?php logo('coingeek.png'); ?>"></li>
				   		<li class="item"><img src="<?php logo('nchain.png'); ?>"></li>
				   		<li class="item"><img src="<?php logo('rawpool.png'); ?>"></li>
				   		<li class="item"><img src="<?php logo('squire.png'); ?>"></li>
				   		<li class="item"><img src="<?php logo('svpool.png'); ?>"></li>
			   		</ul>
				</div>
			</div>
    	</div>
	</div>
</div>