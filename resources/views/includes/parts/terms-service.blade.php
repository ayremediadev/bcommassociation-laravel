<div id="terms-service" class="container" style="padding: 1.8em 3em;">
	<div class="row section-title">
		<h1>Terms of Service</h1>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<strong>Table of Contents</strong>
			<ul class="list-contents">
				<li><a href="#introduction">Introduction</a> </li>
				<li><a href="#obligations">Obligations</a> </li>
				<li><a href="#rights-limits">Rights and Limits</a> </li>
				<li><a href="#disclaimer">Disclaimer and Limit of Liability</a> </li>
				<li><a href="#termination">Termination</a></li>
				<li><a href="#resolution">Governing Law and Dispute Resolution</a> </li>
				<li><a href="#general-terms">General Terms</a></li>
				<li><a href="#dos">bComm Association “Dos and Don’ts”</a> </li>
				<li><a href="#complaints">Complaints Regarding Content </a></li>
				<li><a href="contact-us">How To Contact Us</a> </li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12" id="introduction">
			<h4>1. Introduction</h4>

			<h5>1.1 Contract</h5>
			<p>When you use our Services you agree to all of these terms. Your use of our Services is also subject to our <a href="#">Cookie Policy</a> and our <a href="#">Privacy Policy</a>, which covers how we collect, use, share, and store your personal information.</p>
			<p>You agree that by clicking “Join Now”, “Join bComm Association”, “Sign Up” or similar, registering, accessing or using our services (described below), you are agreeing to enter into a legally binding contract with bComm Association (even if you are using our Services on behalf of a company). If you do not agree to this contract (“Contract” or “User Agreement”), do not click “Join Now” (or similar) and do not access or otherwise use any of our Services. If you wish to terminate this contract, at any time you can do so by closing your account and no longer accessing or using our Service</p>
			<span><strong>Services</strong></span>
			<p>This Contract applies to bComm Association.com, bComm Association-branded apps, bComm Association Learning and other bComm Association-related sites, apps, communications and other services that state that they are offered under this Contract (“Services”), including the offsite collection of data for those Services, such as our ads and the “Apply with bComm Association” and “Share with bComm Association” plugins. Registered users of our Services are “Members” and unregistered users are “Visitors”. This Contract applies to both Members and Visitors.</p>
			<p>This Contract applies to Members and Visitors.</p>
			<p>As a Visitor or Member of our Services, the collection, use and sharing of your personal data is subject to this Privacy Policy (which includes our Cookie Policy and other documents referenced in this Privacy Policy) and updates.</p>

			<h5>1.2 Members and Visitors</h5>
			<p>When you register and join the bComm Association Service, you become a Member. If you have chosen not to register for our Services, you may access certain features as a “Visitor.”</p>
			<h5>1.3 Change</h5>
			<span>We may make changes to the Contract.</span>
			<p>We may modify this Contract, our Privacy Policy and our Cookies Policies from time to time. If we make material changes to it, we will provide you notice through our Services, or by other means, to provide you the opportunity to review the changes before they become effective. We agree that changes cannot be retroactive. If you object to any changes, you may close your account. Your continued use of our Services after we publish or send a notice about our changes to these terms means that you are consenting to the updated terms.</p>

		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" id="obligations">
			<h4>2. Obligations</h4>

			<h5>2.1 Service Eligibility</h5>
			<p>Here are some promises that you make to us in this Contract:</p>
			<p>You’re eligible to enter into this Contract and you are at least our “Minimum Age.”</p>
			<p>The Services are not for use by anyone under the age of 16.</p>
			<p>To use the Services, you agree that: (1) you must be the “Minimum Age” (described below) or older; (2) you will only have one bComm Association account (and/or one SlideShare account, if applicable), which must be in your real name; and (3) you are not already restricted by bComm Association from using the Services. Creating an account with false information is a violation of our terms, including accounts registered on behalf of others or persons under the age of 18.</p>
			<p>“Minimum Age” means 18 years old. However, if law requires that you must be older in order for bComm Association to lawfully provide the Services to you without parental consent (including using of your personal data) then the Minimum Age is such older age.</p>

			<h5>2.2 Your Account</h5>
			<p>You will keep your password a secret.</p>
			<p>You will not share an account with anyone else and will follow our rules and the law.</p>
			<p>Members are account holders. You agree to: (1) try to choose a strong and secure password; (2) keep your password secure and confidential; (3) not transfer any part of your account (e.g., connections) and (4) follow the law and our list of Dos and Don’ts. You are responsible for anything that happens through your account unless you close it or report misuse.</p>
			<p>As between you and others (including your employer), your account belongs to you. However, if the Services were purchased by another party for you to use (e.g. Recruiter seat bought by your employer), the party paying for such Service has the right to control access to and get reports on your use of such paid Service; however, they do not have rights to your personal account.</p>


			<h5>2.3 Notices and Messages</h5>
			<p>You’re okay with us providing notices and messages to you through our websites, apps, and contact information. If your contact information is out of date, you may miss out on important notices.</p>
			<p>You agree that we will provide notices and messages to you in the following ways: (1) within the Service, or (2) sent to the contact information you provided us (e.g., email, mobile number, physical address). You agree to keep your contact information up to date.
			Please review your settings to control and limit messages you receive from us.</p>

			<h5>2.4 Sharing</h5>
			<p>When you share information on our Services, others can see, copy and use that information.</p>
			<p>Our Services allow messaging and sharing of information in many ways, such as your profile, slide decks, links to news articles, job postings, InMails and blogs. Information and content that you share or post may be seen by other Members, Visitors or others (including off of the Services). Where we have made settings available, we will honor the choices you make about who can see content or information (e.g., message content to your addressees, sharing content only to bComm Association connections, restricting your profile visibility from search engines, or opting not to notify others of your bComm Association profile update). For job searching activities, we default to not notifying your connections network or the public. So if you apply for a job through our Service or opt to signal that you are interested in a job, our default is to share it only with the job poster.</p>
			<p>We are not obligated to publish any information or content on our Service and can remove it in our sole discretion, with or without notice.</p>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" id="rights-limits">
			<h4>3. Rights and Limits</h4>
			<h5>3.1. Your License to bComm Association</h5>
			<p>You own all of the content, feedback, and personal information you provide to us, but you also grant us a non-exclusive license to it.</p>
			<p>We’ll honor the choices you make about who gets to see your information and content, including how it can be used for ads.</p>
			<p>As between you and bComm Association, you own the content and information that you submit or post to the Services, and you are only granting bComm Association and our affiliates the following non-exclusive license:</p>
			<p>A worldwide, transferable and sublicensable right to use, copy, modify, distribute, publish, and process, information and content that you provide through our Services and the services of others, without any further consent, notice and/or compensation to you or others. These rights are limited in the following ways:</p>
			<ol>
			<li>You can end this license for specific content by deleting such content from the Services, or generally by closing your account, except (a) to the extent you shared it with others as part of the Service and they copied, re-shared it or stored it and (b) for the reasonable time it takes to remove from backup and other systems.</li>
			<li>We will not include your content in advertisements for the products and services of third parties to others without your separate consent (including sponsored content). However, we have the right, without payment to you or others, to serve ads near your content and information, and your social actions may be visible and included with ads, as noted in the Privacy Policy.</li>
			<li>We will get your consent if we want to give others the right to publish your content beyond the Services. However, if you choose to share your post as "public", we will enable a feature that allows other Members to embed that public post onto third-party services, and we enable search engines to make that public content findable though their services. </li>
			<li>While we may edit and make format changes to your content (such as translating it, modifying the size, layout or file type or removing metadata), we will not modify the meaning of your expression.</li>
			<li>Because you own your content and information and we only have non-exclusive rights to it, you may choose to make it available to others.</li>
			</ol>

			<p>You and bComm Association agree that if content includes personal data, it is subject to our Privacy Policy.</p>
			<p>You and bComm Association agree that we may access, store, process and use any information and personal data that you provide in accordance with the terms of the Privacy Policy and your choices (including settings).</p>
			<p>By submitting suggestions or other feedback regarding our Services to bComm Association, you agree that bComm Association can use and share (but does not have to) such feedback for any purpose without compensation to you.</p>
			<p>You promise to only provide information and content that you have the right to share, and that your bComm Association profile will be truthful.</p>
			<p>You agree to only provide content or information that does not violate the law nor anyone’s rights (including intellectual property rights). You also agree that your profile information will be truthful. bComm Association may be required by law to remove certain information or content in certain countries.</p>
			<h5>3.2 Service Availability</h5>
			<p>We may change, suspend or end any Service, or change and modify prices prospectively in our discretion. To the extent allowed under law, these changes may be effective upon notice provided to you.</p>
			<p>We may change or discontinue any of our Services. We don’t promise to store or keep showing any information and content that you’ve posted.</p>
			<p>bComm Association is not a storage service. You agree that we have no obligation to store, maintain or provide you a copy of any content or information that you or others provide, except to the extent required by applicable law and as noted in our Privacy Policy.</p>
			<h5>3.3 Other Content, Sites and Apps</h5>
			<p>Your use of others’ content and information posted on our Services, is at your own risk.</p>
			<p>Others may offer their own products and services through our Services, and we aren’t responsible for those third-party activities.</p>
			<p>By using the Services, you may encounter content or information that might be inaccurate, incomplete, delayed, misleading, illegal, offensive or otherwise harmful. bComm Association generally does not review content provided by our Members or others. You agree that we are not responsible for others’ (including other Members’) content or information. We cannot always prevent this misuse of our Services, and you agree that we are not responsible for any such misuse. You also acknowledge the risk that you or your organization may be mistakenly associated with content about others when we let connections and followers know you or your organization were mentioned in the news.</p>
			<p>You are responsible for deciding if you want to access or use third-party apps or sites that link from our Services. If you allow a third-party app or site to authenticate you or connect with your bComm Association account, that app or site can access information on bComm Association related to you and your connections. Third-party apps and sites have their own legal terms and privacy policies, and you may be giving others permission to use your information in ways we would not. Except to the limited extent it may be required by applicable law, bComm Association is not responsible for these other sites and apps – use these at your own risk. Please see our <a href="#">Privacy Policy</a>.</p>
			<h5>3.4 Limits</h5>
			<p>We have the right to limit how you connect and interact on our Services.</p>
			<p>bComm Association reserves the right to limit your use of the Services, including the number of your connections and your ability to contact other Members. bComm Association reserves the right to restrict, suspend, or terminate your account if bComm Association believes that you may be in breach of this Contract or law or are misusing the Services.</p>
			<h5>3.5 Intellectual Property Rights</h5>
			<p>We’re providing you notice about our intellectual property rights.</p>
			<p>bComm Association reserves all of its intellectual property rights in the Services. Using the Services does not give you any ownership in our Services or the content or information made available through our Services. Trademarks and logos used in connection with the Services are the trademarks of their respective owners. bComm Association, SlideShare, and “in” logos and other bComm Association trademarks, service marks, graphics, and logos used for our Services are trademarks or registered trademarks of bComm Association.</p>
			<h5>3.6 Automated Processing</h5>
			<p>We use data and information about you to make relevant suggestions to you and others.</p>
			<p>We will use the information and data that you provide and that we have about Members to make recommendations for connections, content and features that may be useful to you. For example, we use data and information about you to recommend jobs to you and you to recruiters. Keeping your profile accurate and up-to-date helps us to make these recommendations more accurate and relevant.</p>

		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" id="disclaimer">
			<h4>4. Disclaimer and Limit of Liability</h4>

			<h5>4.1 No Warranty</h5>
			<p>This is our disclaimer of legal liability for the quality, safety, or reliability of our Services.</p>
			<p>TO THE EXTENT ALLOWED UNDER LAW, BCOMM ASSOCIATION AND ITS AFFILIATES (AND THOSE THAT BCOMM ASSOCIATION WORKS WITH TO PROVIDE THE SERVICES) (A) DISCLAIM ALL IMPLIED WARRANTIES AND REPRESENTATIONS (E.G. WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, ACCURACY OF DATA, AND NONINFRINGEMENT); (B) DO NOT GUARANTEE THAT THE SERVICES WILL FUNCTION WITHOUT INTERRUPTION OR ERRORS, AND (C) PROVIDE THE SERVICE (INCLUDING CONTENT AND INFORMATION) ON AN “AS IS” AND “AS AVAILABLE” BASIS.</p>
			<p>SOME LAWS DO NOT ALLOW CERTAIN DISCLAIMERS, SO SOME OR ALL OF THESE DISCLAIMERS MAY NOT APPLY TO YOU.</p>

			<h5>4.2 Exclusion of Liability</h5>
			<p>These are the limits of legal liability we may have to you.</p>
			<p>TO THE EXTENT PERMITTED UNDER LAW (AND UNLESS BCOMM ASSOCIATION HAS ENTERED INTO A SEPARATE WRITTEN AGREEMENT THAT OVERRIDES THIS CONTRACT), BCOMM ASSOCIATION AND ITS AFFILIATES (AND THOSE THAT BCOMM ASSOCIATION WORKS WITH TO PROVIDE THE SERVICES) SHALL NOT BE LIABLE TO YOU OR OTHERS FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, OR ANY LOSS OF DATA, OPPORTUNITIES, REPUTATION, PROFITS OR REVENUES, RELATED TO THE SERVICES (E.G. OFFENSIVE OR DEFAMATORY STATEMENTS, DOWN TIME OR LOSS, USE OF, OR CHANGES TO, YOUR INFORMATION OR CONTENT).</p>
			<p>IN NO EVENT SHALL THE LIABILITY OF BCOMM ASSOCIATION AND ITS AFFILIATES (AND THOSE THAT BCOMM ASSOCIATION WORKS WITH TO PROVIDE THE SERVICES) EXCEED, IN THE AGGREGATE FOR ALL CLAIMS, AN AMOUNT THAT IS THE LESSER OF (A) FIVE TIMES THE MOST RECENT MONTHLY OR YEARLY FEE THAT YOU PAID FOR A PREMIUM SERVICE, IF ANY, OR (B) US $1000.</p>
			<p>THIS LIMITATION OF LIABILITY IS PART OF THE BASIS OF THE BARGAIN BETWEEN YOU AND BCOMM ASSOCIATION AND SHALL APPLY TO ALL CLAIMS OF LIABILITY (E.G. WARRANTY, TORT, NEGLIGENCE, CONTRACT, LAW) AND EVEN IF BCOMM ASSOCIATION OR ITS AFFILIATES HAS BEEN TOLD OF THE POSSIBILITY OF ANY SUCH DAMAGE, AND EVEN IF THESE REMEDIES FAIL THEIR ESSENTIAL PURPOSE.</p>
			<p>SOME LAWS DO NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY, SO THESE LIMITS MAY NOT APPLY TO YOU.</p>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" id="termination">
			<h4>5. Termination</h4>
			<p>We can each end this Contract anytime we want.</p>
			<p>Both you and bComm Association may terminate this Contract at any time with notice to the other. On termination, you lose the right to access or use the Services. The following shall survive termination:</p>
			<ul>
				<li>Our rights to use and disclose your feedback;</li>
				<li>Members and/or Visitors’ rights to further re-share content and information you shared through the Service to the extent copied or re-shared prior to termination;</li>
				<li>Sections 4, 6, 7, and 8.2 of this Contract;</li>
				<li>Any amounts owed by either party prior to termination remain owed after termination.</li>
			</ul>
			<p>You can visit our <a href="#">Contact Us</a> to close your account.</p>

		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" id="resolution">
			<h4>6. Governing Law and Dispute Resolution</h4>
			<p>In the unlikely event we end up in a legal dispute, bComm Association and you agree to resolve it in Antigua and Barbuda courts, and be governed by the laws of Antigua and Barbuda.</p>

		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" id="general-terms">
			<h4>7. General Terms</h4>
			<p>Here are some important details about the Contract.</p>
			<p>If a court with authority over this Contract finds any part of it unenforceable, you and we agree that the court should modify the terms to make that part enforceable while still achieving its intent. If the court cannot do that, you and we agree to ask the court to remove that unenforceable part and still enforce the rest of this Contract.</p>
			<p>To the extent allowed by law, the English language version of this Contract is binding and other translations are for convenience only. This Contract (including additional terms that may be provided by us when you engage with a feature of the Services) is the only agreement between us regarding the Services and supersedes all prior agreements for the Services.</p>
			<p>If we don't act to enforce a breach of this Contract, that does not mean that bComm Association has waived its right to enforce this Contract. You may not assign or transfer this Contract (or your membership or use of Services) to anyone without our consent. However, you agree that bComm Association may assign this Contract to its affiliates or a party that buys it without your consent. There are no third-party beneficiaries to this Contract.</p>
			<p>You agree that the only way to provide us legal notice is at the addresses provided in Section 10.</p>

		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" id="dos">
			<h4>8. bComm Association “Dos and Don’ts”</h4>
			<h5>8.1. Dos</h5>
			<p>bComm Association is a community of professionals. This list of “Dos and Don’ts” limit what you can and cannot do on our Services.</p>

			<strong>You agree that you will:</strong>
			<ol type="a">
				<li>Comply with all applicable laws, including, without limitation, privacy laws, intellectual property laws, anti-spam laws, export control laws, tax laws, and regulatory requirements;</li>
				<li>Provide accurate information to us and keep it updated;</li>
				<li>Use your real name on your profile; and</li>
				<li>Use the Services in a professional manner.</li>
			</ol>
			<h5>8.2. Don’ts</h5>
			<strong>You agree that you will not:</strong>
			<ol type="a">
				<li>Create a false identity on bComm Association, misrepresent your identity, create a Member profile for anyone other than yourself (a real person), or use or attempt to use another’s account;</li>
				<li>Develop, support or use software, devices, scripts, robots, or any other means or processes (including crawlers, browser plugins and add-ons, or any other technology) to scrape the Services or otherwise copy profiles and other data from the Services;</li>
				<li>Override any security feature or bypass or circumvent any access controls or use limits of the Service (such as caps on keyword searches or profile views);
				d.	Copy, use, disclose or distribute any information obtained from the Services, whether directly or through third parties (such as search engines), without the consent of bComm Association;</li>
				<li>Disclose information that you do not have the consent to disclose (such as confidential information of others (including your employer));</li>
				<li>Violate the intellectual property rights of others, including copyrights, patents, trademarks, trade secrets, or other proprietary rights. For example, do not copy or distribute (except through the available sharing functionality) the posts or other content of others without their permission, which they may give by posting under a Creative Commons license;</li>
				<li>Violate the intellectual property or other rights of bComm Association, including, without limitation, (i) copying or distributing our learning videos or other materials or (ii) copying or distributing our technology, unless it is released under open source licenses; (iii) using the word “bComm Association” or our logos in any business name, email, or URL;</li>
				<li>Post anything that contains software viruses, worms, or any other harmful code;</li>
				<li>Reverse engineer, decompile, disassemble, decipher or otherwise attempt to derive the source code for the Services or any related technology that is not open source;</li>
				<li>Imply or state that you are affiliated with or endorsed by bComm Association without our express consent (e.g., representing yourself as an accredited bComm Association trainer);</li>
				<li>Rent, lease, loan, trade, sell/re-sell or otherwise monetize the Services or related data or access to the same, without bComm Association’s consent;</li>
				<li>Deep-link to our Services for any purpose other than to promote your profile or a Group on our Services, without bComm Association’s consent;</li>
				<li>Use bots or other automated methods to access the Services, add or download contacts, send or redirect messages;</li>
				<li>Monitor the Services’ availability, performance or functionality for any competitive purpose;</li>
				<li>Engage in “framing,” “mirroring,” or otherwise simulating the appearance or function of the Services;</li>
				<li>Overlay or otherwise modify the Services or their appearance (such as by inserting elements into the Services or removing, covering, or obscuring an advertisement included on the Services);</li>
				<li>Interfere with the operation of, or place an unreasonable load on, the Services (e.g., spam, denial of service attack, viruses, gaming algorithms); and/or</li>
				<li>Violate any additional terms concerning a specific Service that are provided when you sign up for or start using such Service.</li>
			</ol>

		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" id="complaints">
			<h4>Complaints Regarding Content</h4>
			<p>Contact information for complaint about content provided by our Members.</p>
			<p>We respect the intellectual property rights of others. We require that information posted by Members be accurate and not in violation of the intellectual property rights or other rights of third parties.</p>

		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" id="contact-us">
			<h4>How To Contact Us</h4>
			<p>Please contact us using <a href="mailto:contact@bcommassociation.com">contact@bcommassociation.com</a></p>

		</div>
	</div>
</div>