<div id="hometop" class="container">

  <div class="row">

    <div class="col col-lg-12">

    	<h5 class="txtupper">bComm Association</h5>

	    <h1 class="txtupper">Building the bcommerce industry,<br> one block at a time.</h1>

	    <p>bComm Association is a global network that brings Merchants, Developers, Miners and Exchanges together within the first-ever platform dedicated to bCommerce.</p>

	    <a class="bttn" href="#community">Read more</a>

    </div>

   <!--  <div class="container squares-dark">

        <div class="row">

          <div class="squares"><i class="fas fa-square-full sq-1 c-dark"></i><i class="fas fa-square-full sq-2 c-dark"></i></div>

        </div>

      </div> -->

  </div>

    <!-- <video loop muted autoplay poster="img/homepage.jpg" class="fullscreen-bg__video">

        <source src="https://bcommassociation.com/laravel-prod/storage/app/img/homepage.jpg" type="video/mp4">


    </video> -->
    <img src="<?php img('homepage.jpg'); ?>" class="fullscreen-bg__video" />

    <div class="opacity-layout"></div>

</div>
