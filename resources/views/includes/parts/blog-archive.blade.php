<?php ?>

<h1 style="margin-top: 1.5em;">News</h1>

<div id="community" class="container">
	<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12 groups">
		<?php $allposts = (array)json_decode(file_get_contents('https://bcommassociation.com/laravel-prod/public/wp/wp-json/wp/v2/posts/'));
		//print_r($allposts); 
			foreach($allposts as $post){ ?>
				<div class="col col-lg-3 col-md-6 col-sm-6 col-xs-12 group">
					<a href="<?php url_replace(); ?>news/<?php echo $post->slug; ?>">
						<?php $imgJson = (array)json_decode(file_get_contents('https://bcommassociation.com/laravel-prod/public/wp/wp-json/wp/v2/media?parent='.$post->id.'')); ?>
							<?php if($imgJson){ ?>
								<img src="<?php echo $imgJson[0]->guid->rendered; ?>" style="width: 100%;"/>
							<?php } else { ?>
								<img src="<?php url_replace(); ?>laravel-prod/public/wp/wp-content/uploads/2018/12/bcomm-header-image.jpg" style="width: 100%;"/>
							<?php } ?>
							<p><?php echo $post->title->rendered; ?></p>
					</a>
				</div>
			<?php } ?>
	</div>
</div>

<?php ?>