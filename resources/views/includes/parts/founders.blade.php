<?php
function imgs($file){
    $base = URL::to('/');
    $path = str_replace("/public","",$base); 
    $img = $path . Storage::url('app/img/founders/' . $file);
    echo $img; 
} 
?>
<div id="founders" class="container">
    <div class="row section-title">
	   <h1 class="txtupper">bComm Association Founding Directors</h1>
    </div>
    <div class="row dirs desktop">   	
    	<div class="col-lg-3 col-md-3 col-xs-12 col-sm-6 dir">
    		<div class="content shadow-md">
                <div class="dir-pic">
	    		     <img src="<?php imgs('craig-wright.jpg');?>">
                </div>
	    		<p class="title txtupper">Craig Wright</p>
	    		<p class="desc">Founder of SVPool.com (Satoshi Vision) & Chief Scientist at nChain Group</p>
    		</div>
    	</div>
    	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dir">
    		<div class="content shadow-md">
                <div class="dir-pic">
	    		     <img src="<?php imgs('jimmy.jpg');?>">
                </div>
	    		<p class="title txtupper">Jimmy Nguyen</p>
	    		<p class="desc">Founding President of bComm Association</p>
    		</div>
    	</div>
    	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dir">
    		<div class="content shadow-md">
                <div class="dir-pic">
	    		     <img src="<?php imgs('james-belding.jpg'); ?>">
                </div>
	    		<p class="title txtupper">James Belding</p>
	    		<p class="desc">Founder of Tokenized</p>
	    	</div>
    	</div>
    	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dir">
    		<div class="content shadow-md">
                <div class="dir-pic">
	    		     <img src="<?php imgs('david-li.jpg');?>">
                </div>
	    		<p class="title txtupper">David Li</p>
	    		<p class="desc">Founder of Rawpool</p>
	    	</div>
    	</div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dir">
            <div class="content shadow-md">
                <div class="dir-pic">
                    <img src="<?php imgs('alexander-shulgin.jpg');?>">
                </div>
                <p class="title txtupper">Alexander Shulgin</p>
                <p class="desc">Visionary, Investor, Composer, Founder & CEO Gruppa Kompaniy Familia</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dir">
            <div class="content shadow-md">
                <div class="dir-pic">
                    <img src="<?php imgs('jerry-chan.jpg');?>">
                </div>
                <p class="title txtupper">Jerry Chan</p>
                <p class="desc">Crypto Advisory Board SBI Digital Asset Holdings</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dir">
            <div class="content shadow-md">
                <div class="dir-pic">
                    <img src="<?php imgs('taras-kulyk.jpg'); ?>">
                </div>
                <p class="title txtupper">Taras Kulyk</p>
                <p class="desc">CEO of Squire Mining Ltd</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dir">
            <div class="content shadow-md">
                <div class="dir-pic">
                    <img src="<?php imgs('calvin-ayre.jpg')?>">
                </div>
                <p class="title txtupper">Calvin Ayre</p>
                <p class="desc">Founder of Coingeek & Coingeek Mining</p>
            </div>
        </div>
    </div>

    <div id="myCarousel" class="row dirs mobile carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
          <li data-target="#myCarousel" data-slide-to="4"></li>
          <li data-target="#myCarousel" data-slide-to="5"></li>
          <li data-target="#myCarousel" data-slide-to="6"></li>
          <li data-target="#myCarousel" data-slide-to="7"></li>
        </ol>      
        <div class="carousel-inner ">
            <div class="col-lg-3 col-md-3 col-xs-12 dir item active">
                <div class="content shadow-md">
                    <div class="dir-pic">
                         <img src="<?php imgs('craig-wright.jpg');?>">
                    </div>
                    <p class="title txtupper">Craig Wright</p>
                    <p class="desc">Founder of SVPool.com (Satoshi Vision) & Chief Scientist at nChain Group</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 dir item">
                <div class="content shadow-md">
                    <div class="dir-pic">
                         <img src="<?php imgs('jimmy.jpg');?>">
                    </div>
                    <p class="title txtupper">Jimmy Nguyen</p>
                    <p class="desc">Founding President of bComm Association</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 dir item">
                <div class="content shadow-md">
                    <div class="dir-pic">
                         <img src="<?php imgs('james-belding.jpg'); ?>">
                    </div>
                    <p class="title txtupper">James Belding</p>
                    <p class="desc">Founder of Tokenized</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 dir item">
                <div class="content shadow-md">
                    <div class="dir-pic">
                         <img src="<?php imgs('david-li.jpg');?>">
                    </div>
                    <p class="title txtupper">David Li</p>
                    <p class="desc">Founder of Rawpool</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 dir item">
                <div class="content shadow-md">
                    <div class="dir-pic">
                        <img src="<?php imgs('alexander-shulgin.jpg');?>">
                    </div>
                    <p class="title txtupper">Alexander Shulgin</p>
                    <p class="desc">Visionary, Investor, Composer, Founder & CEO Gruppa Kompaniy Familia</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 dir item">
                <div class="content shadow-md">
                    <div class="dir-pic">
                        <img src="<?php imgs('jerry-chan.jpg');?>">
                    </div>
                    <p class="title txtupper">Jerry Chan</p>
                    <p class="desc">Crypto Advisory Board SBI Digital Asset Holdings</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 dir item">
                <div class="content shadow-md">
                    <div class="dir-pic">
                        <img src="<?php imgs('taras-kulyk.jpg'); ?>">
                    </div>
                    <p class="title txtupper">Taras Kulyk</p>
                    <p class="desc">CEO of Squire Mining Ltd</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 dir item">
                <div class="content shadow-md">
                    <div class="dir-pic">
                        <img src="<?php imgs('calvin-ayre.jpg')?>">
                    </div>
                    <p class="title txtupper">Calvin Ayre</p>
                    <p class="desc">Founder of Coingeek & Coingeek Mining</p>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
         </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
    </div>
</div>

