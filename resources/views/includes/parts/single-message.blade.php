<?php
    function pics($file){
        $base = URL::to('/');
        $path = str_replace("/public","",$base);
        $img = 'https://bcommassociation.com/laravel-prod/' . Storage::url('app/img/' . $file);
        echo $img; 
    } 

    if (Auth::check()) {
		$unread_messages = Auth::user()->unread_messages;	
        $avatar = Auth::user()->avatar;
    }
    else {
        $avatar = NULL; 
    }
    function avatar_app($file){
        $img = 'https://bcommassociation.com/laravel-prod/storage/app/avatars/' . $file;
        echo $img; 
    } 
	
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NX5V6DG');</script>
<!-- End Google Tag Manager -->
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-client_id" content="434810445866-n46h7vlj606mjepl9apjupiehboqtv14.apps.googleusercontent.com">
    <link rel="shortcut icon" href="https://bcommassociation.com/assets/images/favicon.ico" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <?php $baseURL = URL::to('/') . '/'; 
    $path = str_replace("/public","",$baseURL); 
    //$path = $path . 'laravel-prod/'; ?>
    <div class="test" style="display: none"><?php echo $path;?></div>

    <script>
        function clicked() {
            var $myForm = document.getElementById("updateInfo");
            $myForm.find(':submit').click();
        }
        function submittedUpdate(){
            alert("Personal information updated");
        }
    </script>
    <script>
        $(document).ready(function(){
            $('#editButton').click(function(){
                var $form = $('#updateInfo');
                var $button = $('#editButton');
                $(this).toggleClass('editing'); 
                if($(this).hasClass('editing')){
                    $('#updateInfo input').attr('readonly', false);
                    $('#updateInfo select').attr('disabled', false);
                    $('#updateInfo textarea').attr('readonly', false);
                    $(this).html('Done'); 
                } else {
                    $('#updateInfo input').attr('readonly', true);
                    $('#updateInfo select').attr('disabled', true);
                    $('#updateInfo textarea').attr('readonly', true);
                    $(this).html('<i class="fas fa-pencil-alt"></i>Edit'); 
                }
            });
             // add submit form filtered
            $("#filter-profile :input[type='checkbox']").attr('onchange','this.form.submit()');
        });
    </script>
    <script>
        $(document).ready(function(){
            $('#editInt').click(function(){
                var $form = $('.updateInt');
                var $button = $('#editInt');
                $(this).toggleClass('editing'); 
                if($(this).hasClass('editing')){ 
                    $('.updateInt button').attr('disabled', false);
                    $(this).html('Done'); 
                } else { 
                    $('.updateInt button').attr('disabled', true); 
                    $(this).html('<i class="fas fa-pencil-alt"></i>Edit'); 
                } 
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $('form.updateInt').submit(function(){
                $(this).find('button').toggleClass('btn-outline-gradient-sm-green');
                event.preventDefault();
                $(this).submit();
            });
        });
    </script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://bcommassociation.com/laravel-prod/template/resources/css/app-mid.css">
    <link rel="stylesheet" type="text/css" href="https://bcommassociation.com/laravel-prod/template/style.css">
    <link rel="stylesheet" type="text/css" href="https://bcommassociation.com/laravel-prod/style.css">
    <link rel="stylesheet" href="https://bcommassociation.com/laravel-prod/resources-templates/css/circle.css">
    <link rel="stylesheet" href="https://bcommassociation.com/laravel-prod/resources-templates/css/template-style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,700" rel="stylesheet">

	<!-- trigger upload button in edit profile -->
<script>
    $(document).ready(function(){
        
        jQuery("i.fa-camera").click(function(){
          $("input[type='file']").trigger('click');
        });

        jQuery('input[type="file"]').on('change', function() {
            $('#update_avatar').submit();
            var val = $(this).val();
            $(this).siblings('span').text(val);
        });
    });
</script>

</head>
<body>
	
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NX5V6DG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div id="app">
        @if(Auth::guest())
			
          @else
		  
                    <main class="py-4" style="margin-top: 0; padding-top: 15px !important;">
					
					
					<?php 
$baseURL = 'https://bcommassociation.com'; 
 $csspath = 'https://bcommassociation.com';
 $base = URL::to('/');
  $path = str_replace("/public","",$base); 

function imgs($file) {
  $base = 'https://bcommassociation.com';
  //$path = str_replace("/public","",$base); 
  $img = $base . Storage::url('app/img/' . $file);
  echo $img; 
} 
function avatar($file) {
  $base = 'https://bcommassociation.com';
  //$path = str_replace("/public","",$base); 
  $img = $base . "/laravel-prod" . Storage::url('app/avatars/' . $file);
  echo $img; 
}

Route::get('user/profile', ['as' => 'my-profile', 'uses' => 'UserController@profile']);

$id = Auth::user()->id;
$name = Auth::user()->name;
$email = Auth::user()->email;
$position = Auth::user()->position;
$company = Auth::user()->company;
$location = Auth::user()->location;
$avatar = Auth::user()->avatar;
$tagline = Auth::user()->tagline; 
$presentation = Auth::user()->presentation; 
$bchprofile = Auth::user()->bch_profile; 
$interests = Auth::user()->interests; 
$opportunities = Auth::user()->opportunities; 
$experiences = Auth::user()->experiences; 
$connections = Auth::user()->connections; 
$phone = Auth::user()->phone; 
$messages = Auth::user()->messages;

?>

  <main role="main" id="profile" style="margin-top: 0;">

        <section class="profile">

            <div class="container" style="padding-top: 2em 0 !important; margin: 0 !important; width: 100%; max-width: 100%;">
        
                <div class="row">
					
					<?php 
						$msg = DB::table('bcomm_messages')->where('id', '=', $_GET["msg"])->first();
						$fromID = $msg->user_id_1; 
						$from = DB::table('users')->where('id', '=', $fromID)->first(); 
					?>
					
					<div class="col-md-12">
						<h2><img style="max-width: 35px; margin-right: 1em; float: left;" src="<?php pics('back.png') ?>" />
							<?php echo $msg->subject; ?></h2>
						<h4>From <em><?php echo $from->name; ?></em> (<?php echo $msg->sent; ?>)</h4>
						<br/><br/>
						<?php echo '<div><p>' . $msg->message . '</p></div>'; 
						if($msg->type == 'connect_request'){ ?>
							
							<img src="<?php avatar_app($from->avatar); ?>"/><br/> 
						
							<?php echo '<form target="updateconnection" method="get" action="https://bcommassociation/confirmconnect" style="display: inline;">
									<input type="hidden" value="2" name="connection" />
									<input type="hidden" value="'.$fromID.'" name="user_id_1" />
									<input type="hidden" value="'.$id.'" name="user_id_2" />
									<input type="submit" value="Confirm" />
								</form>';
								
							echo '<form target="updateconnection" method="get" action="https://bcommassociation/confirmconnect" style="display: inline;">
									<input type="hidden" value="0" name="connection" />
									<input type="hidden" value="'.$fromID.'" name="user_id_1" />
									<input type="hidden" value="'.$id.'" name="user_id_2" />
									<input type="submit" value="Decline" />
								</form>';
							echo '<iframe src="https://bcommconnection/confirmconnect" name="updateconnection" id="updateconnection" style="width: 600px; height: 600px;"></iframe>';
						} ?>
					</div>
					
				</div>
				
				<div class="row">
					
                    <div class="col-md-12">
					
						<?php if($_GET['msg']){
							
							
							
						} else { 
						
						} ?>

                    </div>

                </div>
    
            </div>

        </section>
    
  </main>
					
					
                    </main>
          @endif
    </div>
</body>
</html>
