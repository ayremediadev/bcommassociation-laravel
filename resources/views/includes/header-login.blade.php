<section id="header">
    <div class="navbar">
        <div class="navbar-inner row" style="text-align: center;">
            <div class="col-md-5 col-lg-5 col-xl-5 col-sm-3 col-xs-3">
            </div>
            <div id="loginLogoDiv" class="col-md-2 col-lg-2 col-xl-2 col-sm-6 col-xs-6">
                <a id="logo" href="/laravel/public/">            
                    <?php $baseURL = URL::to('/');
                    $imgURL = str_replace("public","",$baseURL); ?>
                    <?php $LogoUrl = $imgURL . Storage::url('app/img/bcomm-rgb-logo-light.svg'); ?>
                    <img style="width: 65%; margin: auto;" id="logoimage" src="<?php echo $LogoUrl; ?>" />
                </a>
            </div>
            <div class="col-md-5 col-lg-5 col-xl-5 col-sm-3 col-xs-3">
            </div>
        </div>
    </div>
</section>