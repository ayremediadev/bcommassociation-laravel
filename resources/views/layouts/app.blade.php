<?php
    function pics($file){
        $base = URL::to('/');
        $path = str_replace("/public","",$base);
        $img = 'https://bcommassociation.com/laravel-prod/' . Storage::url('app/img/' . $file);
        echo $img; 
    } 

    if (Auth::check()) {
		$unread_messages = Auth::user()->unread_messages;	
        $avatar = Auth::user()->avatar;
    }
    else {
        $avatar = NULL; 
    }
    function avatar_app($file){
        $img = 'https://bcommassociation.com/laravel-prod/storage/app/avatars/' . $file;
        echo $img; 
    } 

if(Auth::guest()){
	
// do nothing

} else {
	
$id = Auth::user()->id;
$name = Auth::user()->name;
$email = Auth::user()->email;
$position = Auth::user()->position;
$company = Auth::user()->company;
$location = Auth::user()->location;
$avatar = Auth::user()->avatar;
$tagline = Auth::user()->tagline; 
$presentation = Auth::user()->presentation; 
$bchprofile = Auth::user()->bch_profile; 
$interests = Auth::user()->interests; 
$opportunities = Auth::user()->opportunities; 
$experiences = Auth::user()->experiences; 
$connections = Auth::user()->connections; 
$phone = Auth::user()->phone; 
$messages = Auth::user()->messages;
$unread_messages = Auth::user()->unread_messages;	

} 
	
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NX5V6DG');</script>
<!-- End Google Tag Manager -->
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://bcommassociation.com/assets/images/favicon.ico" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <?php $baseURL = URL::to('/') . '/'; 
    $path = str_replace("/public","",$baseURL); 
    //$path = $path . 'laravel-prod/'; ?>
    <div class="test" style="display: none"><?php echo $path;?></div>

    <script>
        function clicked() {
            var $myForm = document.getElementById("updateInfo");
            $myForm.find(':submit').click();
        }
        function submittedUpdate(){
            alert("Personal information updated");
        }
    </script>
    <script>
        $(document).ready(function(){
            $('#editButton').click(function(){
                var $form = $('#updateInfo');
                var $button = $('#editButton');
                $(this).toggleClass('editing'); 
                if($(this).hasClass('editing')){
                    $('#updateInfo input').attr('readonly', false);
                    $('#updateInfo select').attr('disabled', false);
                    $('#updateInfo textarea').attr('readonly', false);
                    $(this).html('Done'); 
                } else {
                    $('#updateInfo input').attr('readonly', true);
                    $('#updateInfo select').attr('disabled', true);
                    $('#updateInfo textarea').attr('readonly', true);
                    $(this).html('<i class="fas fa-pencil-alt"></i>Edit'); 
                }
            });
             // add submit form filtered
            $("#filter-profile :input[type='checkbox']").attr('onchange','this.form.submit()');
        });
    </script>
    <script>
        $(document).ready(function(){
            $('#editInt').click(function(){
                var $form = $('.updateInt');
                var $button = $('#editInt');
                $(this).toggleClass('editing'); 
                if($(this).hasClass('editing')){ 
                    $('.updateInt button').attr('disabled', false);
                    $(this).html('Done'); 
                } else { 
                    $('.updateInt button').attr('disabled', true); 
                    $(this).html('<i class="fas fa-pencil-alt"></i>Edit'); 
                } 
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $('form.updateInt').submit(function(){
                $(this).find('button').toggleClass('btn-outline-gradient-sm-green');
                event.preventDefault();
                $(this).submit();
            });
        });
    </script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://bcommassociation.com/laravel-prod/template/resources/css/app-mid.css">
    <link rel="stylesheet" type="text/css" href="https://bcommassociation.com/laravel-prod/template/style.css">
    <link rel="stylesheet" type="text/css" href="https://bcommassociation.com/laravel-prod/style.css">
    <link rel="stylesheet" href="https://bcommassociation.com/laravel-prod/resources-templates/css/circle.css">
    <link rel="stylesheet" href="https://bcommassociation.com/laravel-prod/resources-templates/css/template-style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,700" rel="stylesheet">

	<!-- trigger upload button in edit profile -->
<script>
    $(document).ready(function(){
        
        jQuery("i.fa-camera").click(function(){
          $("input[type='file']").trigger('click');
        });

        jQuery('input[type="file"]').on('change', function() {
            $('#update_avatar').submit();
            var val = $(this).val();
            $(this).siblings('span').text(val);
        });
    });
</script>

</head>
<body>
	
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NX5V6DG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div id="app">
        @if(Auth::guest())
            <main class="py-4  login-screen">
            <div class="container h-login-screen">
                    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-charcoal col-lg-12">

                        <div class="col-lg-12 col-sm-12 logo centered">
                            <a href="https://bcommassociation.com/"><i class="fas fa-arrow-left"></i></a>
                            <a class="navbar-brand navbar-brand align-items-center" href="https://bcommassociation.com/">
                                <!-- {{ config('app.name', 'Laravel') }} -->
                                <img src="<?php pics('bcomm-rgb-logo-light.svg') ?>">
                            </a>
                        </div>
                    </nav>
            </div>
            
                @yield('content')
            </main>
          @else
                    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-charcoal col-lg-12">
                        <div class="col-lg-3 col-sm-9 logo">
                            <a class="navbar-brand navbar-brand d-flex align-items-center" href="https://bcommassociation.com/home">
                                <!-- {{ config('app.name', 'Laravel') }} -->
                                <img src="<?php pics('bcomm-rgb-logo-light.svg') ?>">
                            </a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse col-lg-9 col-md-9 col-sm-12" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                             <ul class="navbar-nav ml-auto" style="margin-left: 0 !important;">
                                  <!--<li class="nav-item">
                                    <a class="nav-link" href="#">Community</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="#">Membership</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="#">About</a>
                                  </li>-->
								  <li class="nav-item">
                                    <a class="nav-link" href="https://bcommassociation.com/home">Home</a>
                                  </li>
								  <li class="nav-item">
                                    <a class="nav-link" href="https://bcommassociation.com/members">Members</a>
                                  </li>
								  <li class="nav-item">
                                    <a class="nav-link" href="https://bcommassociation.com/mail">Messages<?php if($unread_messages > 0){ ?><span style="border-radius: 50%; background: #fff; color: #000; padding: .5em .7em .5em .5em; margin-left: 5px; border-radius: 0.8em; -moz-border-radius: 0.8em; -webkit-border-radius: 0.8em; font-weight: bold; width: 1.6rem; text-align: center;"?> <?php echo $unread_messages; ?> </span><?php } ?></a>
                                  </li>
								  <li class="nav-item">
                                    <a class="nav-link" href="https://bcommassociation.com/news">News</a>
                                  </li>
                             </ul>


                            <!-- Right Side Of Navbar -->
                            <ul class="navbar-nav ml-auto">
                                <!-- Authentication Links -->
                                @guest
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        @if (Route::has('register'))
                                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                        @endif
                                    </li>
                                @else
                                    <li class="nav-item navbar-user dropdown">
                                       <!-- <div class="user-info">
                                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                <spna class="user-name">{{ Auth::user()->name }} </spna> <span class="caret"></span>
                                            </a>

                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </div>
                                        </div>
                                        <div class="user-pic pic-sm">
                                            <img src="<?php //pics('user-1.jpg') ?>"/>
                                        </div> -->
                                        <div class="d-flex">
                                            <div class="user-info">
                                              <div class="text-white font-weight-bold text-uppercase">{{ Auth::user()->name }}</div>
                                              <div class="user-links text-white text-uppercase font-weight-light"><a href="https://bcommassociation.com/home">My Account</a>
                                                | <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                              </div>
                                            </div>
                                            <div class="user-pic pic-sm">
                                              <img src="<?php avatar_app($avatar) ?>">
                                            </div>
                                        </div>
                                    </li>
                                @endguest
                            </ul>
                     </div>
                    </nav>
                    <main class="py-4">
                        @yield('content')
                    </main>
          @endif
    </div>
<script type="text/javascript">
var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq ||
{widgetcode:"5e57af61e8b5a52cf86004c14cde29339f430f7fb9e01d79f72e868e0211078a8a0234aa19c9aa56f0ce085ffb3e7c5b", values:{},ready:function(){}};
var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;
s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");
</script>
</body>
</html>
