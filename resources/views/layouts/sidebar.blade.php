
<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body style="margin: 0;">
<div class="container" style="width: 100%;">

    <header class="row">
        @include('includes.header')
    </header>

    <div id="main" class="row">

        <!-- main content -->
        <div id="content" class="col-md-8">
            @yield('content')
        </div>

    </div>

    <footer class="row">
        @include('includes.footer')
    </footer>

</div>
</body>
</html>

