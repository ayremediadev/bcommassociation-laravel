<!doctype html>
<html>
<head>
	
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NX5V6DG');</script>
<!-- End Google Tag Manager -->
   
   <style>
		h1, figure { width: 100%; text-align: center; }  
		figure { margin-bottom: 2rem !important; } 
		#remote-content { max-width: 90vw; margin: auto; }
		#remote-date { text-align: center; width: 100%; margin-bottom: 2rem; } 
   </style>
   
	@include('includes.head-api')

<?php 

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
$slug = substr($actual_link, 33);

if(strpos($slug, '?fbclid=') !== false){
	
	$post_slug = substr($slug, 0, strpos($slug, '?fbclid='));
	
} else {
	
	$post_slug = $slug; 
	
}
	
$allposts = (array)json_decode(file_get_contents('https://bcommassociation.com/laravel-prod/public/wp/wp-json/wp/v2/posts/'));

$postid = '';
foreach($allposts as $post){

	$slug = $post->slug;

	if($slug == $post_slug){

		$postid = $post->id;

		$jsondata = (array)json_decode(file_get_contents('https://bcommassociation.com/laravel-prod/public/wp/wp-json/wp/v2/posts/'.$postid.'/'));
		$jsondata2 = (array)json_decode(file_get_contents('https://bcommassociation.com/laravel-prod/public/wp/wp-json/wp/v2/posts/'.$postid.'?_embed'));

		$titleArray = (array)$jsondata['title']; 
		$title = $titleArray['rendered'];

		$jsonBFIarray = (array)$jsondata2['better_featured_image'];
		$featured_image_url = $jsonBFIarray['source_url'];

		$jsonExcerptArray = (array)$jsondata2['excerpt'];
		$excerpt = $jsonExcerptArray['rendered'];
		?>
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:site" content="@bCommWorld" />
		<meta name="twitter:creator" content="@bCommWorld" />
		<meta property="og:url" content="<?php echo $actual_link; ?>" />
		<meta property="og:title" content="<?php echo $title; ?>" />
		<meta property="og:description" content="<?php echo $excerpt; ?>" />
		<meta property="og:image" content="<?php echo $featured_image_url; ?>" />
		<?php
	} 
}
?>

</head>
<body style="margin: 0;">
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NX5V6DG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="container" id="body-container" style="padding: 0; width: 97.5%; margin: auto;">

    <header class="row">
        @include('includes.header')
    </header>

    <div id="main" class="row blog" style="padding: 0; width: 100vw;">

            @yield('content')
			
			<br/><br/><br/>

            @include('includes.footer')
        
    </div>
	
</div>
<script type="text/javascript">
var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq ||
{widgetcode:"5e57af61e8b5a52cf86004c14cde29339f430f7fb9e01d79f72e868e0211078a8a0234aa19c9aa56f0ce085ffb3e7c5b", values:{},ready:function(){}};
var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;
s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");
</script>
</body>
</html>