<!doctype html>
<html>
<head>
	
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NX5V6DG');</script>
<!-- End Google Tag Manager -->
   
	@include('includes.head')
	
</head>
<body style="margin: 0;">
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NX5V6DG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	
<div class="container" id="body-container" style="padding: 0; width: 97.5%; margin: auto;">

    <header class="row">
        @include('includes.header')
    </header>

    <div id="main" class="row" style="padding: 0; width: 100vw;">

            @yield('content')

            @include('includes.footer')
        
    </div>

</div>
<script type="text/javascript">
var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq ||
{widgetcode:"5e57af61e8b5a52cf86004c14cde29339f430f7fb9e01d79f72e868e0211078a8a0234aa19c9aa56f0ce085ffb3e7c5b", values:{},ready:function(){}};
var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;
s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");
</script>
</body>
</html>