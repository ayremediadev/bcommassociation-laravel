
<!doctype html>
<html>
<head>
	
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NX5V6DG');</script>
<!-- End Google Tag Manager -->
	
    @include('includes.head')
</head>
<body style="margin: 0;">
	
		<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NX5V6DG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="container" id="body-container" style="padding: 0;margin: 0 !important;width: 100%;">

    <header class="row">
        @include('includes.header')
    </header>

    <div id="main" class="row" style="width: 100%;">

            @yield('content')

    </div>

        @include('includes.footer')

</div>
</body>
</html>